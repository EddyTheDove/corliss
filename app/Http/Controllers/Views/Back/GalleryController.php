<?php

namespace App\Http\Controllers\Views\Back;

use App\Models\Product;
use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class GalleryController extends Controller
{
    


    public function store(Request $request)
    {
    	$rules = [
        	'product_id' => 'required',
            'gallery'    => 'required'        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());



        $product = Product::find($request->product_id);
        if(!$product)
    		return redirect()->back()
                         	 ->withInput($request->all())
                         	 ->withErrors(['Product' => 'Product not found! Cannot add the item']);


        $gallery = Gallery::create([
        	'rank' 		 => (int) $request->rank > 0 ? $request->rank : 1,
        	'image' 	 => $request->gallery,
        	'product_id' => $request->product_id
        ]);

        return redirect()->back()->with('message', 'Image added to this gallery');
    }


    public function destroy($id)
    {
    	$image = Gallery::find($id);
    	if(!$image)
    		return redirect()->to('/one/products');

    	$image->delete();
    	return redirect()->back()->with('message', 'The image has been deleted');
    }
}
