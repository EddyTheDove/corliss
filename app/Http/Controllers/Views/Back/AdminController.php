<?php

namespace App\Http\Controllers\Views\Back;

use Auth;
use Cache;
use Input;
use App\Models\Order;
use App\Models\User;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    public function home()
    {
    	if(Auth::check() && Auth::user()->isAdmin())
    	{
            $orders     = Order::where('status', 'placed')->count();
            $deliveries = Order::where('status', 'dispatched')
                               ->orWhere('status', 'delivered')->count();

            $products = Product::all()->count();
            $users = User::all()->count();

    		return view('one.dashboard', compact('products', 'users', 'orders', 'deliveries'));
    	}

    	return view('one.user.login');
    }



    public function signin(Request $request)
    {
    	$email = $request->email;
    	$password = $request->password;

    	if(Auth::attempt(['email' => $email, 'password' => $password, 'status' => 'active', 'level' => 0]))
    		return redirect()->to('/one');

    	return redirect('/one')->withErrors(["email" => "Please check your credentials"])
    						   ->withInput($request->only('email'));
    }


    public function signout()
    {
    	if(Auth::check())
    		Auth::logout();
    	return redirect()->to('one');
    }





    public function files()
    {
        return view('one.product.files');
    }
}
