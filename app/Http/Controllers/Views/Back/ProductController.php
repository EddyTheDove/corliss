<?php

namespace App\Http\Controllers\Views\Back;

use DB;
use Auth;
use Cache;
use Input;
use App\Models\Size;
use App\Models\Colour;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{


    public function index()
    {

    	$products = Product::orderBy('id', 'desc')->paginate(50);
    	return view('one.product.index', compact('products'));
    }


    public function create()
    {
    	$categories = Category::where('parent', false)->get();

    	return view('one.product.create', compact('categories'));
    }



    public function show($sku)
    {
        $product = Product::where('sku', $sku)->first();

        if(!$product)
            return redirect()->to('one/products');

        $sizes   = Size::orderBy('short')->get();
        $colours = Colour::orderBy('name')->get();

        return view('one.product.show', compact('product', 'sizes', 'colours'));
    }



    public function store(Request $request)
    {
    	$rules = [
        	'name'      => 'required',
            'status'    => 'required',
            'has_gst'   => 'required',
            'max_order' => 'required',
            'in_stock'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());


        if(!$request->categories)
        		return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors(['category' => 'You need to assign at least one category to the product']);


        $sku = 103218;
        if($lastProduct = Product::orderBy('id', 'desc')->first())
        	$sku = $lastProduct->sku += rand(7, 29);

        /**
         * add slug here
         * @var slug
         */
        $product = Product::create([
        	'sku'		  => $sku,
        	'image'		  => $request->image,
        	'price'		  => $request->price,
        	'name'		  => $request->name,
        	'slug'		  => $request->slug,
        	'sale'		  => $request->sale ? $request->sale : 0,
        	'status'	  => $request->status,
        	'in_stock'	  => $request->in_stock,
        	'has_gst'	  => $request->has_gst,
        	'max_order'	  => $request->max_order,
        	'is_featured' => $request->is_featured,
        	'description' => $request->description
        ]);

        if($product) {
            $arr = [];
        	foreach($request->categories as $category) {
                $arr[] = ['product_id' => $product->id, 'category_id' => $category];
        	}

            DB::table('product_categories')->insert($arr);
        }

        return redirect()->to('/one/products')->with('message', 'New product added!');
    }





    public function edit($sku)
    {
    	$product = Product::where('sku', $sku)->first();

    	if(!$product)
    		return redirect()->to('one/products');

    	$categories = Category::where('parent', false)->get();
    	return view('one.product.edit', compact('product', 'categories'));
    }







    public function update(Request $request, $id)
    {
    	$rules = [
        	'name'      => 'required',
            'status'    => 'required',
            'has_gst'   => 'required',
            'max_order' => 'required',
            'in_stock'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());


        if(!$request->categories)
        		return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors(['category' => 'You need to assign at least one category to the product']);

        $product = Product::find($id);
        if(!$product)
    		return redirect()->to('one/products');

        $product->name          = $request->name;
        $product->slug          = $request->slug;
    	$product->image 		= $request->image;
    	$product->price 		= $request->price;
    	$product->sale 			= $request->sale ? $request->sale : 0;
    	$product->status		= $request->status;
    	$product->in_stock		= $request->in_stock;
    	$product->has_gst		= $request->has_gst;
    	$product->max_order		= $request->max_order;
    	$product->is_featured	= $request->is_featured;
    	$product->description	= $request->description;

    	$product->save();

    	$product->categories()->detach();

    	foreach($request->categories as $category)
    	{
    		DB::table('product_categories')->insert([
    			'product_id'	=> $product->id,
    			'category_id'	=> $category
    		]);
    	}


    	return redirect()->back()->with('message', 'Product updated!');
    }
}
