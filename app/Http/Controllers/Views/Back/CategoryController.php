<?php

namespace App\Http\Controllers\Views\Back;

use Auth;
use Cache;
use Input;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Category;
use App\Models\Functions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    
    public function index()
    {
    	$categories = Category::where('parent', false)->paginate(50);
    	return view('one.category.index', compact('categories'));
    }



    /*
    ** Add a new category
    */
    public function store(Request $request)
    {
    	$rules = [
        	'name'  => 'required',
            'slug'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());

        


        $slugExists = Category::where('slug', $request->slug)->first();
        if($slugExists)
        	return redirect()->back()
        					 ->withErrors(['slug' => 'That slug already exists'])
        					 ->withInput($request->all());


        $category = Category::create([
        	'name'	=> $request->name,
        	'slug'	=> $request->slug,
        	'description' => $request->description
        ]);

        if($request->parent){
        	$parent = $request->parent;
        
        	$cat = Category::find($parent);

        	if($cat)
        	{
        		$category->parent = $parent;
	        	$category->save();
        	}
        	
        }

        return redirect()->back()->with('message', 'Category added');
    }




    public function edit($id)
    {
    	$category = Category::find($id);

    	if(!$category)
    		return redirect()->to('/one/categories');

    	$categories = Category::where('parent', false)->get();
    	return view('one.category.edit', compact('category', 'categories'));
    }



    /*
	** UPDATE A CATEGORY
	*/
    public function update(Request $request, $id)
    {
    	$rules = [
        	'name'  => 'required',
            'slug'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());

        $category = Category::find($id);
        if(!$category)
        	return redirect()->to('/one/categories');

        $category->name 	= $request->name;
        $category->slug 	= Functions::link($request->slug);
        $category->parent 	= (int) $request->parent;
        $category->description = $request->description;

        $category->save();
        return redirect()->back()->with('message', 'Category updated!');
    }



	/*
	** DELETE A CATEGORY
	*/
    public function destroy($id)
    {
    	$category = Category::find($id);

    	if(!$category)
    		return redirect()->to('/one/categories');


    	if(count($category->children))
    	{
    		foreach($category->children as $child)
    		{
    			if(count($child->products))
    				return redirect()->back()->withErrors(['child' => 'One of the sub categories has some products']);
    		}

    		if(!count($category->products))
    		{
    			foreach($category->children as $child)
	    		{
	    			$child->delete();
	    		}
	    		$category->delete();
    		}
    	}
    	else
    	{
    		if(count($category->products)){
    			return redirect()->back()->withErrors(['products' => 'This category has some products']);
    		}
    		else{
    			$category->delete();
    		}
    	}


    	
    	return redirect()->to('/one/categories')->with('message', 'Category was deleted');
    }
}
