<?php

namespace App\Http\Controllers\Views\Back;

use Auth;
use Cache;
use Input;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    
	public function index(){
	    $orders = Order::paginate(50);
	    return view('one.order.index', compact('orders'));
	}
}
