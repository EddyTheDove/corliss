<?php

namespace App\Http\Controllers\Views\Back;

use DB;
use Auth;
use Cache;
use App\Models\Size;
use App\Models\Colour;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VariationController extends Controller
{
    public function index()
    {
    	$sizes   = Size::all();
    	$colours = Colour::all();

    	return view('one.variation.index', compact('sizes', 'colours'));
    }


    public function colourStore(Request $request)
    {
    	$rules = [
        	'name'      => 'required',
            'value'    => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());


        $colour = Colour::create([
        	'name'	=> $request->name,
        	'value' => $request->value
        ]);

        return redirect()->back()->with('message', 'Colour added');
    }


    public function colourDestroy($id)
    {
    	$colour = Colour::find($id);
    	if(!$colour)
    		return redirect()->to('/one/variations');

    	if($colour->items->count()){
    		return redirect()->back()
    						 ->withErrors(["colour" => "The colour cannot be deleted because it contains items"]);
    	}

    	$colour->delete();
    	return redirect()->to('/one/variations')->with('message', 'The colour has been deleted');
    }










    /**SIZES**/
    public function sizeStore(Request $request)
    {
    	$rules = [
        	'name'      => 'required',
            'short'    => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());


        Size::create([
        	'name'	=> $request->name,
        	'short' => $request->short
        ]);

        return redirect()->back()->with('size', 'Colour added');
    }


    public function sizeDestroy($id)
    {
    	$size = Size::find($id);
    	if(!$size)
    		return redirect()->to('/one/variations');

    	if($size->items->count()){
    		return redirect()->back()
    						 ->withErrors(["size" => "The size cannot be deleted because it contains items"]);
    	}

    	$size->delete();
    	return redirect()->to('/one/variations')->with('message', 'The size has been deleted');
    }
}
