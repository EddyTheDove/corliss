<?php

namespace App\Http\Controllers\Views\Back;



use App\Models\Item;
use App\Models\Size;
use App\Models\Colour;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
    
    public function store(Request $request)
    {
    	$rules = [
        	'colour_id' => 'required',
            'size_id'   => 'required'        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());



        $product = Product::find($request->product_id);
        if(!$product)
    		return redirect()->back()
                         	 ->withInput($request->all())
                         	 ->withErrors(['Product' => 'Product not found! Cannot add the item']);


        $item = Item::create([
        	'rank' 		 => (int) $request->rank > 0 ? $request->rank : 1,
        	'sale' 		 => $request->sale  ? $request->sale : $product->sale,
        	'price' 	 => $request->price ? $request->price : $product->price,
        	'image' 	 => $request->image ? $request->image : $product->image,
        	'size_id' 	 => $request->size_id,
        	'colour_id'  => $request->colour_id,
        	'product_id' => $request->product_id,
        	'quantity' 	 => (int) $request->quantity
        ]);

        $product->quantity += $request->quantity;
        $product->save();

        return redirect()->back()->with('message', 'Item added to this product');
    }




    public function edit($id)
    {
    	$item = Item::find($id);
    	if(!$item)
    		return redirect()->to('/one/products');

    	$sizes   = Size::orderBy('short')->get();
        $colours = Colour::orderBy('name')->get();

        return view('one.product.item', compact('item', 'sizes', 'colours'));
    }





    public function update(Request $request, $id)
    {
    	$rules = [
        	'colour_id' => 'required',
            'size_id'   => 'required'        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());



        $item = Item::find($id);
        if(!$item)
    		return redirect()->to('/one/products')
                         	 ->withErrors(['Item' => 'Not an item']);


        $item->sale  = $request->sale;
        $item->price = $request->price;
        $item->rank  = $request->rank;
        $item->image = $request->image;
        $item->size_id = $request->size_id;
        $item->colour_id = $request->colour_id;

        $item->save();

        return redirect()->back()->with('message', 'Item updated');
    }




    public function updateStock(Request $request)
    {
    	$rules = [
        	'item_id' => 'required',
            'quantity'   => 'required'        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        	return redirect()->back()
                             ->withInput($request->all())
                             ->withErrors($validator->errors()->all());



        $item = Item::find($request->item_id);
        if(!$item)
    		return redirect()->to('/one/products')
                         	 ->withErrors(['Item' => 'Not an item']);


        $item->quantity  += $request->quantity;
        $item->product->quantity += $request->quantity;

        $item->save();
        $item->product->save();

        return redirect()->back()->with('message', 'Stock updated');
    }
}
