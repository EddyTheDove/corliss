<?php

namespace App\Http\Controllers\Views\Front;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{

    /**
     * display user's invoice
     *
     * @param  $number
     *
     * @return view()
     */
    public function show($number)
    {
        $order = '';
        return view('front.user.order', compact('order'));
    }



    /**
     * show user's bag
     *
     * @return view
     */
    public function bag()
    {

        return view('front.user.bag');
    }



    /**
     *  checkout & payment
     *
     * @return view
     */
    public function checkout()
    {
        $user = Auth::user();
        $states = DB::table('states')->get();
        return view('front.user.checkout', compact('states', 'user'));
    }



    /**
     *  Order invoice
     *
     * @return view
     */
    public function invoice()
    {

        return view('front.user.invoice');
    }
}
