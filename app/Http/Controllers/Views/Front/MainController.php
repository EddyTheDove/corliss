<?php

namespace App\Http\Controllers\Views\Front;

use DB;
use Auth;
use Cache;
use App\Models\User;
use App\Models\Product;
use App\Models\Functions;
use App\Models\Categories;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    /**
     * display home page
     *
     * @return view
     */
    public function home()
    {

        $prime = Product::where('status', true)
        ->orderBy('id', 'desc')
        ->first();

        $products = Product::where('status', true)
        ->orderBy('id', 'desc')
        ->skip(1)
        ->take(5)
        ->get();

        //check if the user is connected
        if ( Auth::check() ) {
            foreach ( Auth::user()->favourites as $userFav ) {
                if ( $userFav->id === $prime->id ) { //check if the prime is a favourite
                    $prime->is_favourite = true;
                }

                foreach ( $products as $product ) {
                    if ( $product->id === $userFav->id ) { //check if the product is a favourite
                        $product->is_favourite = true;
                    }
                }
            }
        }

        return view('front.pages.home', compact('prime', 'products'));
    }





    public function index()
    {
        $products = Product::orderBy('id', 'desc')
                  ->where('status', true)
                  ->where('in_stock', true)
                  ->take(6)->get();

        return view('frontend.index', compact('products'));
    }


    public function signin()
    {
    	if(Auth::check())
    		return redirect()->to('/');

    	return view('front.pages.signin');
    }

    public function signout()
    {
        if(Auth::check())
            Auth::logout();

        return redirect()->to('signin');
    }


    public function activate($code)
    {
    	if(Auth::check())
            return redirect()->to('/');

        $user = User::where('code', $code)->first();
        if(!$user || $user->status == 'active')
            return redirect()->to('/');

        return view('front.user.activate', compact('user'));
    }
}
