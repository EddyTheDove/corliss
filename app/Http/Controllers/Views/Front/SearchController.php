<?php

namespace App\Http\Controllers\Views\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $result = '';

        return view('front.products.search', compact('result'));
    }
}
