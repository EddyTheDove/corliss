<?php

namespace App\Http\Controllers\Views\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * about page
     *
     * @return view()
     */
    public function about()
    {
        return view('front.pages.about');
    }



    /**
     * contact page
     *
     * @return view()
     */
    public function contact()
    {
        return view('front.pages.contact');
    }


    /**
     * terms page
     *
     * @return view()
     */
    public function terms()
    {
        return view('front.pages.terms');
    }


    /**
     * privacy page
     *
     * @return view()
     */
    public function privacy()
    {
        return view('front.pages.privacy');
    }


    /**
     * returns page
     *
     * @return view()
     */
    public function returns()
    {
        return view('front.pages.returns');
    }


    /**
     * delivery page 
     *
     * @return view()
     */
    public function delivery()
    {
        return view('front.pages.delivery');
    }
}
