<?php

namespace App\Http\Controllers\Views\Front;

use DB;
use Auth;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{


    /**
     * display first hand page
     *
     * @var  Request $request
     *
     * @return view
     */
    public function firstHand(Request $request)
    {
        return view('front.products.first');
    }


    /**
     * display first hand page
     *
     * @var  Request $request
     *
     * @return view
     */
    public function secondHand(Request $request)
    {
        return view('front.products.second');
    }



    /**
     * show single product
     *
     * @var product sku
     *
     * @return view
     */
    public function show($sku)
    {

        return view('front.products.show');
    }






    /**
     *  User's favourites
     *
     * @return view
     */
    public function favs()
    {

        return view('front.user.favs');
    }
}
