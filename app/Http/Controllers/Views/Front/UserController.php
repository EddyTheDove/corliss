<?php

namespace App\Http\Controllers\Views\Front;

use DB;
use Auth;
use Hash;
use Mail;
use Input;
use Cache;
use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /**
     * user's registration
     *
     * @param $request
     *
     * @return redirect()
     */
    public function register(Request $request)
    {

        $rules = [
            'firstname'     => 'required',
            'lastname'      => 'required',
            'email'         => 'required|email|unique:users'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
            return redirect()->back()->withErrors($validator);


        $number = 103466;

        if($lastCode = User::orderBy('id', 'desc')->first())
            $number = $lastCode->number += rand(13, 27);

        $user = User::create([
            'code'          => \Uuid::generate(),
            'number'        => $number,
            'firstname'     => $request->firstname,
            'lastname'      => $request->lastname,
            'email'         => $request->email,
            'password'      => Hash::make('password'),
            'status'		=> 'inactive',
            'api_token'     => str_random(60)
        ]);



        Mail::send('emails.activate', ['user' => $user], function($message)
        use ($user)
        {
            $message->to($user->email, $user->firstname)
                ->subject('Welcome to Berlis')
                ->from('no-reply@berlis.com.au', 'Berlis');
        });

        return redirect()->back()->with('message', "Congrats! We have emailed you an activation link. Please check your inbox. And your junk mails prehaps");
    }




    /**
     * User's authentication
     *
     * @param  Request $request
     *
     * @return redirect()
     */
    public function authenticate(Request $request)
    {
        $email     = $request->get('email');
        $password  = $request->get('password');


        $user = User::where('email', $email)->first();
        if(!$user){

            return redirect()
                   ->back()
                   ->withInput($request->only('email'))
                   ->withErrors(["User" => "Email not found. Please create an account"]);
        }

        if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 'active'], true)) {
            return redirect()->intended('/account');
        }

        return redirect()
            ->back()
            ->withInput($request->only('email'))
            ->withErrors([
                'email' => 'Please check your credentials'
            ]);
    }





    /**
     * account activation
     *
     * @param  Request $request
     *
     * @return redirect()
     */
    public function activate(Request $request)
    {
        $token = $request->get('token');
        $user = User::where('code', $token)->first();

        if(!$user){
            return redirect()->back()->withErrors(["Hummmm" => "Uknown token"]);
        }

        $rules = [
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails())
            return redirect()->back()->withErrors($validator);


        if($user->status == 'blocked')
            return redirect()->back();

        $user->password = Hash::make($request->get('password'));
        $user->status = 'active';
        $user->update();

        Auth::login($user);
        return redirect()->to('/')->with('message', 'Well done! We would like to know you a bit more about you.');
    }






    /**
     * account update
     *
     * @param $request
     *
     * @return redirect()
     */
    public function accountUpdate(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'firstname'     => 'required',
            'lastname'      => 'required'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $user->firstname    = $request->firstname;
        $user->lastname     = $request->lastname;
        $user->email        = $request->email;
        $user->phone        = $request->phone;
        $user->address      = $request->address;
        $user->suburb       = $request->suburb;
        $user->state        = $request->state;
        $user->postcode     = $request->postcode;
        $user->save();

        return redirect()->back()->with('message', "Your account has been updated!");
    }




    /**
     * user's account
     *
     * @return view()
     */
    public function account()
    {
        if ( !Auth::check() ) {
            return redirect()->to('/');
        }
        $user = Auth::user();
        $states = DB::table('states')->get();

        return view('front.user.account', compact('user', 'states'));
    }


    /**
     * user signs out
     *
     * @return redirect()
     */
    public function signout()
    {
        if ( Auth::check() ) {
            Auth::logout();
        }
        return redirect()->to('/');
    }
}
