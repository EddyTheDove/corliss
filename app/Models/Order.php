<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
    	'cart_id',
    	'user_id',
    	'status',
    	'total'
    ];

    
}
