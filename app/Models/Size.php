<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $table = 'sizes';
    protected $fillable = [
        'name',
        'short',
        'description'
    ];


    public function items(){
    	return $this->hasMany('App\Models\Item');
    }
}
