<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $table = 'promos';
    protected $dates = [
        'expiry'
    ];

    protected $fillable = [
        'user_id', 'order_id', 'amount', 'units', 'is_public', 'expiry'
    ];
}
