<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'sku',
    	'name',
        'sale',
        'tags',
    	'slug',
    	'image',
    	'price',
    	'status',
        'has_gst',
    	'in_stock',
        'quantity',
        'max_order',
    	'description'
    ];

    public function url() {
        return $this->sku . '/'. $this->slug;
    }

    public function categories(){
    	return $this->belongsToMany('App\Models\Category', 'product_categories');
    }


    public function items(){
        return $this->hasMany('App\Models\Item')->orderBy('rank')->orderBy('id', 'desc');
    }


    public function hasItems(){
        return $this->hasMany('App\Models\Item')->count();
    }


    public function gallery(){
        return $this->hasMany('App\Models\Gallery')->orderBy('rank')->orderBy('id');
    }

    public function hasGallery(){
        return $this->hasMany('App\Models\Gallery')->count();
    }
}
