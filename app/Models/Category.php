<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   
    protected $table = 'categories';
    protected $fillable = [
    	'name',
    	'slug',
    	'parent',
    	'description'
    ];


    public function products(){
    	return $this->belongsToMany('App\Models\Product', 'product_categories')->orderBy('id', 'desc');
    }

    public function dad(){
    	return $this->belongsTo('App\Models\Category', 'parent');
    }

    public function children(){
    	return $this->hasMany('App\Models\Category', 'parent', 'id');
    }
}
