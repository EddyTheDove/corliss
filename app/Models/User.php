<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'firstname',
        'lastname',
        'postcode',
        'password',
        'address',
        'number',
        'suburb',
        'email',
        'phone',
        'state',
        'level',
        'code'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];



    public function isAdmin(){
        return $this->level == 0;
    }

    public function favourites() {
        return $this->belongsToMany('App\Models\Product', 'favourites');
    }
}
