<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $fillable = [
        'product_id',
        'size_id',
        'colour_id',
        'quantity',
        'price',
        'image',
        'sale',
        'rank'
    ];


    public function product(){
    	return $this->belongsTo('App\Models\Product');
    }

    public function size(){
    	return $this->belongsTo('App\Models\Size');
    }

    public function colour(){
    	return $this->belongsTo('App\Models\Colour');
    }


    public function thumb(){
        return str_replace("/images/", "/thumbs/", $this->image);
    }
}
