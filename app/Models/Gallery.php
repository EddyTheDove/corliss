<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'galleries';
    protected $fillable = [
    	'image',
    	'rank',
    	'product_id'
    ];

    public function product(){
    	return $this->belongsTo('App\Models\Product');
    }

    public function thumb(){
        return str_replace("/images/", "/thumbs/", $this->image);
    }
}
