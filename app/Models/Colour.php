<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Colour extends Model
{
    protected $table = 'colours';
    protected $fillable = [
        'name',
        'value',
        'rgb'
    ];


    public function items(){
    	return $this->hasMany('App\Models\Item');
    }
}
