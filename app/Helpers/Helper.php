<?php
namespace App\Helpers;
use DB;

/**
 * Create this class in its namespace
 * Then add an alias to /config/app.php
 * 'Helper' => App\Helpers\Helper::class,
 */

class Helper
{

    public static getStates()
    {
        return DB::table('states')->all();
    }



    public static randomPromoCode()
    {
        return str_random(6);
    }


}
