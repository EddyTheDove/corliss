var gulp    = require('gulp'),
    sass    = require('gulp-sass'),
    strip   = require('gulp-strip-css-comments'),
    useref  = require('gulp-useref'),
    uglify  = require('gulp-uglify'),
    gulpIf  = require('gulp-if'),
    eslint  = require('gulp-eslint'),
    concat  = require('gulp-concat'),
    cssnano = require('gulp-cssnano'),
    runSequence = require('run-sequence');


//compile sass
gulp.task('sass', function() {
    return gulp.src('./resources/assets/sass/app.scss')
    .pipe(sass())
    .pipe(concat('app.min.css'))
    .pipe(strip())
    .pipe(cssnano())
    .pipe(gulp.dest('public/assets/css'))
});


//copy js files from vendor
gulp.task('vendorjs', function() {
    var source = require('./vendorjs.json');
    return gulp.src(source)
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/assets/js'))
});

//compile angular files
gulp.task('angular', function() {
    var root = 'resources/assets/js';
    var source = [
        'env.js',
        root + '/app.js',
        root + '/modules/**/*module.js',
        root + '/modules/**/*service.js',
        root + '/controllers/**/*.js',
        root + '/directives/**/*.js',
    ];

    return gulp.src(source)
    .pipe(concat('app.min.js'))
    //.pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('public/assets/js'))
});


//watch
gulp.task('watch', ['vendorjs', 'sass', 'angular'], function() {
    gulp.watch('./resources/assets/sass/**/*.scss', ['sass']);
    gulp.watch('./resources/assets/js/**/*.js', ['angular']);
});

//default task
gulp.task('default', function(callback) {
    runSequence(['sass','vendorjs', 'angular', 'watch'], callback)
});
