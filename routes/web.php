<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Auth routes
Route::get('login', 'Views\Front\MainController@signin');
Route::get('signout', 'Views\Front\UserController@signout');
Route::post('login', 'Views\Front\UserController@authenticate');
Route::post('register', 'Views\Front\UserController@register');
Route::get('/activate/{code}', 'Views\Front\MainController@activate');
Route::post('/activate', 'Views\Front\UserController@activate');


//static pages
Route::get('terms', 'Views\Front\PageController@terms');
Route::get('about', 'Views\Front\PageController@about');
Route::get('contact', 'Views\Front\PageController@contact');
Route::get('returns', 'Views\Front\PageController@returns');
Route::get('privacy', 'Views\Front\PageController@privacy');
Route::get('delivery', 'Views\Front\PageController@delivery');


//product pages
Route::get('/', 'Views\Front\MainController@home');
Route::get('first-hand', 'Views\Front\ProductController@firstHand');
Route::get('second-hand', 'Views\Front\ProductController@secondHand');
Route::get('search', 'Views\Front\SearchController@search');

Route::group(['prefix' => 'products'], function() {
    Route::get('{sku}/{slug}', 'Views\Front\ProductController@show');
});



//middleware
Route::get('bag', 'Views\Front\OrderController@bag');



Route::group(['middleware' => 'auth'], function() {
    Route::get('account', 'Views\Front\UserController@account');
    Route::post('account', 'Views\Front\UserController@accountUpdate');
    Route::get('favourites', 'Views\Front\ProductController@favs');
    Route::get('orders/{number}', 'Views\Front\OrderController@show');

    Route::get('checkout', 'Views\Front\OrderController@checkout');
    Route::get('invoice', 'Views\Front\OrderController@invoice');
    Route::resource('users', 'Views\Front\UserController');
});





/**
* Admin routes
*/
Route::get('one', 'Views\Back\AdminController@home');
Route::post('one', 'Views\Back\AdminController@signin');

Route::group(['middleware' => ['admin_auth', 'admin'], 'prefix' => 'one'], function(){
    Route::get('signout', 'Views\Back\AdminController@signout');
    Route::get('files', 'Views\Back\AdminController@files');

    Route::get('variations', 'Views\Back\VariationController@index');
    Route::get('variations/colours/{id}/delete', 'Views\Back\VariationController@colourDestroy');
    Route::get('variations/sizes/{id}/delete', 'Views\Back\VariationController@sizeDestroy');
    Route::post('variations/colours', 'Views\Back\VariationController@colourStore');
    Route::post('variations/sizes', 'Views\Back\VariationController@sizeStore');
    Route::post('items/stock', 'Views\Back\ItemController@updateStock');
    Route::get('galleries/{id}/delete', 'Views\Back\GalleryController@destroy');

    Route::resource('items', 'Views\Back\ItemController');
    Route::resource('orders', 'Views\Back\OrderController');
    Route::resource('products', 'Views\Back\ProductController');
    Route::resource('galleries', 'Views\Back\GalleryController');
    Route::resource('categories', 'Views\Back\CategoryController');
});
