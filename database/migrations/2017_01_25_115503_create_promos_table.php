<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->integer('user_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('amount')->default(10);
            $table->integer('minimum')->default(0);
            $table->boolean('status')->default(true);
            $table->boolean('is_public')->default(false);
            $table->dateTime('expiry');
            $table->enum('units', ['percent', 'amount'])->default('percent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}
