<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sku');
            $table->string('name');
            $table->string('image');
            $table->string('tags')->nullable();
            $table->string('slug')->nullable();
            $table->decimal('price', 5,2)->default(0);
            $table->decimal('sale', 5,2)->default(0);
            $table->boolean('status')->default(false);
            $table->boolean('in_stock')->default(false);
            $table->boolean('has_gst')->default(true);
            $table->boolean('is_featured')->default(false);
            $table->integer('max_order')->default(4);
            $table->integer('quantity')->default(0);
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
