<?php

use Illuminate\Database\Seeder;

class ColourTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('colours')->insert([
            'value' => '#f00',
            'name'  => 'Red'
        ]);

        \DB::table('colours')->insert([
            'value' => '#08c',
            'name'  => 'Blue'        ]);
    }
}
