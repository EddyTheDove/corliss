<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            [
                'code' => 'act',
                'name' => 'Australian Capital Territory'
            ],
            [
                'code' => 'nsw',
                'name' => 'New South Wales'
            ],
            [
                'code' => 'qld',
                'name' => 'Queensland'
            ],
            [
                'code' => 'sa',
                'name' => 'South Australia'
            ],
            [
                'code' => 'tas',
                'name' => 'Tasmania'
            ],
            [
                'code' => 'vic',
                'name' => 'Victoria'
            ],
            [
                'code' => 'wa',
                'name' => 'Western Australia'
            ]
        ]);
    }
}
