<?php

use Illuminate\Database\Seeder;

class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('sizes')->insert([
            'short'  => 'S',
            'name'    => 'Small',
            'description' => 'For skinny mothafuckas'
        ]);

        \DB::table('sizes')->insert([
            'short'  => 'M',
            'name'    => 'Medium',
            'description' => 'For assholes like me'
        ]);
    }
}
