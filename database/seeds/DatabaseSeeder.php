<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(ColourTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(PromoTableSeeder::class);
        $this->call(SizeTableSeeder::class);
    }
}
