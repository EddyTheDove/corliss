<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
        	'code'		=> \Uuid::generate(),
        	'number'	=> 103423,
        	'firstname' => 'Amanda',
        	'lastname'	=> 'Miller',
        	'email'		=> 'amanda@email.com',
        	'password'	=> Hash::make('pass'),
            'status'    => 'active',
            'api_token' => str_random(60)
        ]);

        \DB::table('users')->insert([
            'code'      => \Uuid::generate(),
            'number'    => 103445,
            'firstname' => 'Lil',
            'lastname'  => 'Mary',
            'email'     => 'lil@email.com',
            'password'  => Hash::make('pass'),
            'status'    => 'active',
            'level'     => 0,
            'api_token' => str_random(60)
        ]);

        \DB::table('users')->insert([
        	'code'		=> \Uuid::generate(),
        	'number'	=> 103449,
        	'firstname' => 'User',
        	'lastname'	=> 'Account',
        	'email'		=> 'user@email.com',
        	'password'	=> Hash::make('pass'),
            'status'    => 'active',
            'api_token' => str_random(60)
        ]);

        \DB::table('users')->insert([
            'code'      => \Uuid::generate(),
            'number'    => 103452,
            'firstname' => 'Admin',
            'lastname'  => 'Account',
            'email'     => 'admin@email.com',
            'password'  => Hash::make('pass'),
            'status'    => 'active',
            'level'     => 0,
            'api_token' => str_random(60)
        ]);
    }
}
