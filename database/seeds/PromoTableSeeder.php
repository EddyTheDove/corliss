<?php

use Illuminate\Database\Seeder;
use App\Models\Promo;
use Carbon\Carbon;

class PromoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Promo::create([
            'code'      => 'Promo2017',
            'amount'    => 15,
            'is_public' => true,
            'expiry'    => Carbon::now()->addDays(30)
        ]);
    }
}
