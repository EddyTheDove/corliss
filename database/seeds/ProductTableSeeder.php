<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->insert([
            'sku'  => 103218,
            'name'  => "Nice Smelly Dress",
            'slug'  => "nice-smelly-dress",
            'image'  => "http://corliss.dev/docs/images/New/dresses/nice-smelly-dress.jpg",
            'price'	=> 29.90,
            'sale'	=> 0.00,
            'status'	=> 1,
            'in_stock'	=> 1,
            'has_gst'	=> 1,
            'max_order' => 4,
            'tags'      => 'nice, nice smelly, nice dress,',
            'description'    => 'lorem ipsum dolor',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        \DB::table('products')->insert([
            'sku'  => 103234,
            'name'  => "Grey Anatomy SKirt",
            'slug'  => "grey-anatomy-skirt",
            'image'  => "http://corliss.dev/docs/images/New/dresses/3.jpg",
            'price'	=> 20.00,
            'sale'	=> 15.50,
            'status'	=> 1,
            'in_stock'	=> 1,
            'has_gst'	=> 1,
            'max_order' => 4,
            'tags'      => 'grey, grey anatomy, grey skirt,',
            'description'    => 'lorem ipsum dolor',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        \DB::table('product_categories')->insert([
            [
                'product_id'  => 1,
                'category_id' => 1
            ],
            [
                'product_id'  => 1,
                'category_id' => 2
            ],
            [
                'product_id'  => 2,
                'category_id' => 1
            ],
            [
                'product_id'  => 2,
                'category_id' => 3
            ]
        ]);


        \DB::table('favourites')->insert([
            'user_id' => 3,
            'product_id' => 1
        ]);


    }
}
