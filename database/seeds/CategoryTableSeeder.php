<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->insert([
            'parent'  => 0,
            'name'    => 'First Hand',
            'slug'    => 'first-hand',
            'description'    => 'lorem ipsum dolor'
        ]);

        \DB::table('categories')->insert([
            'parent'  => 1,
            'name'    => 'Dresses',
            'slug'    => 'new-dresses',
            'description'    => 'lorem ipsum dolor'
        ]);

        \DB::table('categories')->insert([
            'parent'  => 1,
            'name'    => 'Skirts',
            'slug'    => 'new-shoes',
            'description'    => 'lorem ipsum dolor'
        ]);
    }
}
