<section class="berlish-product-similar hidden-xs">
    <div class="container">
        <h2 class="text-center">Other girls also bought</h2>


        <div class="row mt-20">
            <div class="col-sm-3 col-md-2">
                <a href="">
                    <div class="similar-item">
                        <div class="img">
                            <img src="/assets/img/01.jpg" alt="Berlish" class="img-responsive">
                        </div>
                        <h4>$18.50</h4>

                        <div class="title">
                            Bitchy face girl
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-sm-3 col-md-2">
                <a href="">
                    <div class="similar-item">
                        <div class="img">
                            <img src="/assets/img/02.jpg" alt="Berlish" class="img-responsive">
                        </div>
                        <h4>$45.70</h4>

                        <div class="title">
                            Long grey shit to the knees
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-sm-3 col-md-2">
                <a href="">
                    <div class="similar-item">
                        <div class="img">
                            <img src="/assets/img/03.jpg" alt="Berlish" class="img-responsive">
                        </div>
                        <h4>$24.00</h4>

                        <div class="title">
                            Short grey shit to the neck
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-sm-3 col-md-2">
                <a href="">
                    <div class="similar-item">
                        <div class="img">
                            <img src="/assets/img/04.jpg" alt="Berlish" class="img-responsive">
                        </div>
                        <h4>$9.99</h4>

                        <div class="title">
                            Ugly pinky shirt no one wants to buy
                        </div>
                    </div>
                </a>
            </div>


            <div class="col-sm-3 col-md-2 hidden-sm">
                <a href="">
                    <div class="similar-item">
                        <div class="img">
                            <img src="/assets/img/05.jpg" alt="Berlish" class="img-responsive">
                        </div>
                        <h4>$69.70</h4>

                        <div class="title">
                            Long dress hiding skinny legs
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-sm-3 col-md-2 hidden-sm">
                <a href="">
                    <div class="similar-item">
                        <div class="img">
                            <img src="/assets/img/06.jpg" alt="Berlish" class="img-responsive">
                        </div>
                        <h4>$15.22</h4>

                        <div class="title">
                            Cheap ear for sale
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </div>
</section>
