<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand offcanvas-toggle" href="#" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas">
                <i class="flaticon-menu-button"></i> Berlish
            </a>
        </div>




        <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/favourites"><i class="flaticon-like"></i> <span>Favourites</span></a></li>
                <li><a href="/bag"><i class="flaticon-shopping-bag"></i> <span>Shopping Bag</span></a></li>
                <li>
                    <a href="/account">
                        <i class="flaticon-social-1"></i>
                        <span>My Account</span>
                    </a>
                </li>
            </ul>
            <div class="nav-search hidden-xs">
                <form class="" method="get" action="/search">
                    <input type="text" name="q" class="input-search" placeholder="What are you looking for?" required>
                    <i class="flaticon-magnifying-glass"></i>
                </form>
            </div>
        </div>
    </div>
</nav>
