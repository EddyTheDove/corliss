<section class="home-newsleter-row">
    <div class="container">
        <div class="">
            <h2 class="no-padding">Sign up for weekly updates</h2 class="no-margin">

            <form action="" method="get" class="">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="">
                            <input type="email" name="email" class="input-grey" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <button type="submit" name="submit" class="btn btn-blue btn-lg btn-block">
                            <i class="flaticon-note"></i> Subscribe
                        </button>
                    </div>
                </div>
            </form>

            <div class="fs-16 mt-40">
                Always check our emails. They sometimes come with coupon codes.
            </div>
        </div>
    </div>
</section>
