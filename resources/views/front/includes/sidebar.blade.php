<!-- Sidebar -->
<sidebar class="clearfix ">
    <nav class="navbar navbar-default navbar-offcanvas navbar-offcanvas-touch navbar-offcanvas-fade" role="navigation" id="js-bootstrap-offcanvas">

        <div class="navbar-header">
            <a class="navbar-brand offcanvas-toggle pointer" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas">
                <i class="flaticon-menu-button"></i> Berlish
            </a>
        </div>


        <div class="side-search">
            <form class="" action="" method="post">
                <div class="form-group">
                    <input type="text" name="search" role="search" placeholder="Search" class="side-search-input">
                </div>
            </form>
        </div>


        <ul class="nav navbar-nav">
            <h4>Filter</h4>
            <form class="" action="" method="post">
                <div class="ml-10">
                    <div class="mt-10">
                        <label class="css-input css-radio css-radio-lg css-radio-success fs-14">
                            <input type="radio" name="cat" value="first_hand" checked required>
                            <span></span><b>First Hand</b>
                        </label>
                    </div>
                </div>

                <div class="ml-40">
                    <div class="mt-5">
                        <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                            <input type="checkbox" name="sub[]" value="dresses"><span></span> Dresses
                        </label>
                    </div>
                    <div class="mt-5">
                        <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                            <input type="checkbox" name="sub[]" value="skirts"><span></span> Skirts
                        </label>
                    </div>
                    <div class="mt-5">
                        <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                            <input type="checkbox" name="sub[]" value="tops"><span></span> Tops
                        </label>
                    </div>
                    <div class="mt-5">
                        <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                            <input type="checkbox" name="sub[]" value="accessories"><span></span> Accessories
                        </label>
                    </div>
                </div>


                <div class="ml-10">

                    <div class="mt-5">
                        <label class="css-input css-radio css-radio-lg css-radio-success fs-14">
                            <input type="radio" name="cat" value="second_hand" required>
                            <span></span><b>Second Hand</b>
                        </label>
                    </div>
                </div>

                <div class="ml-40">
                    <div class="mt-5">
                        <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                            <input type="checkbox" name="sub[]" value="dresses"><span></span> Dresses
                        </label>
                    </div>
                    <div class="mt-5">
                        <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                            <input type="checkbox" name="sub[]" value="tops"><span></span> Tops
                        </label>
                    </div>
                    <div class="mt-5">
                        <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                            <input type="checkbox" name="sub[]" value="accessories"><span></span> Accessories
                        </label>
                    </div>
                </div>

                <div class="pd-10 mt-20">
                    <button type="submit" class="btn btn-lg btn-block btn-green">
                        <i class="flaticon-controls"></i> Filter
                    </button>
                </div>

            </form>
        </ul>

    </nav>
</sidebar>
<!-- Sidebar -->
