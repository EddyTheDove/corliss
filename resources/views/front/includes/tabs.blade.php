
<section class="home-tabs">
    <div class="container">
        <ul class="list-inline">
            <li class="{{ Request::is('/') ? 'active hidden-xs' : '' }}">
                <a href="/">Home</a>
            </li>
            <li class="{{ Request::is('first-hand') ? 'active hidden-xs' : '' }}">
                <a href="/first-hand">First Hand</a>
            </li>
            <li class="{{ Request::is('second-hand') ? 'active hidden-xs' : '' }}">
                <a href="/second-hand">Second Hand</a>
            </li>
        </ul>
    </div>
</section>
