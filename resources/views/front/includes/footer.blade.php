<footer>
    <div class="container">
        <div class="footer-brand">
            BERLISH
        </div>


        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="footer-menu-item">
                    <h4>Information</h4>
                    <ul class="list-unstyled">
                        <li><a href="/about">About Us</a></li>
                        <li><a href="/terms">Terms and conditions</a></li>
                        <li><a href="/privacy">Privay Policy</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-3 col-md-3">
                <div class="footer-menu-item">
                    <h4>Help</h4>
                    <ul class="list-unstyled">
                        <li><a href="/account">Account</a></li>
                        <li><a href="/delivery">Delivery</a></li>
                        <li><a href="/returns">Returns</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-3 col-md-3">
                <div class="footer-menu-item">
                    <h4>Delivery Options</h4>
                    <ul class="list-unstyled">
                        <li><a href>Sydney => $5</a></li>
                        <li><a href>NSW Suburbs => $10</a></li>
                        <li><a href>Australia => $15</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-3 col-md-3">
                <div class="footer-menu-item">
                    <ul class="list-unstyled">
                        <li>
                            <a href=""><div class="logo-insta"></div></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="mt-20 mb-40 text-center">
            <ul class="list-inline logos">
                <li><img src="/assets/img/logos/logos.png"></li>
            </ul>
        </div>

        <div class="text-center copyright">&copy; {{ date('Y') }} Berlish Pty Ltd</div>
        <div class="text-center author">.</div>
    </div>
</footer>
