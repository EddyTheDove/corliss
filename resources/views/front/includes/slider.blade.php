<section class="home-slider">
    <div id="myCarousel" class="carousel slide">
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for carousel items -->
        <div class="carousel-inner">
            <div class="active item">
                <img src="/assets/design/608398.jpg" alt="First Slide">
         		<div class="carousel-caption">
                  <h3>Cool Spring Collection</h3>
                  <p>Colourful collection of dresses for Spring</p>
                  <div class="pb-10">
                      <a href="" class="btn btn-lg btn-pink">
                          <i class="flaticon-redo"></i> View Collection
                      </a>
                  </div>
                </div>
            </div>
            <div class="item">
                <img src="/assets/design/608399.jpg" alt="Second Slide">
                <div class="carousel-caption">
                  <h3>Get Noticed</h3>
                  <p>The choice is now yours. Don't Wait !</p>
                  <div class="pb-10">
                      <a href="" class="btn btn-lg btn-white">
                          <i class="flaticon-redo"></i> Get It Now
                      </a>
                  </div>
                </div>
            </div>
            <div class="item">
                <img src="/assets/design/6966263.jpg" alt="Third Slide">
                <div class="carousel-caption">
                  <h3>Welcome to marvelous Spring</h3>
                  <p>The most colourful season of the year</p>
                </div>
            </div>
        </div>
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left flaticon-arrows"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right flaticon-arrows-3"></span>
        </a>
    </div>
</section>
