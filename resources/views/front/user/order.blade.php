@extends('front.private')


@section('head')
    <title>Invoice - Berlish</title>
@endsection


@section('body')
    <section class="bag-page">
        <div class="container">
            <div class="row mt-40">
                <div class="col-sm-12 col-md-9">
                    <div class="bag-card">
                        <div class="card-header">
                            <h3 class="text-right">Invoice No 45729813</h3>
                            <h4 class="text-right">14/02/2017</h4>
                        </div>

                        <div class="row mt-40">
                            <div class="col-sm-6">
                                <div class="pd-10">
                                    <h4>Billed To:</h4>
                                    <h3>Liscors Kya Iy</h3>
                                    <ul class="list-unstyled fs-18">
                                        <li>67 Macarthur St,</li>
                                        <li>Ultimo, NSW 2007</li>
                                    </ul>
                                </div>
                            </div>
                            <!--Billed to-->

                            <div class="col-sm-6">
                                <div class="pd-10">
                                    <h4>Issued By</h4>
                                    <h3>Berlish Pty Ltd</h3>
                                    <ul class="list-unstyled fs-18">
                                        <li>67 Macarthur St,</li>
                                        <li>Ultimo, NSW 2007</li>
                                    </ul>
                                </div>
                            </div>
                            <!--Issued by-->
                        </div>



                        <div class="items">
                            <table class="table table-striped">
                                <colgroup>
                                    <col class="col-xs-9">
                                    <col class="col-xs-3">
                                </colgroup>

                                <thead>
                                    <tr class="fs-18 bold">
                                        <th>Item Description</th>
                                        <th>Cost</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr data-href='/products/11312338/product-slug'>
                                        <td>1 x Grey boring shirt for winter</td>
                                        <td class="text-right">$24.80</td>
                                    </tr>

                                    <tr data-href='/products/11312337/product-slug'>
                                        <td>1 x Pinky large neck shirt</td>
                                        <td class="text-right">$9.70</td>
                                    </tr>

                                    <tr data-href='/products/11312336/product-slug'>
                                        <td>1 x That cheap cheap shirt</td>
                                        <td class="text-right">$2.00</td>
                                    </tr>

                                    <tr data-href='/products/11312335/product-slug'>
                                        <td>1 x The bitchy face girl</td>
                                        <td class="text-right">$0.99</td>
                                    </tr>

                                    <tr>
                                        <td class="text-right bold fs-20">Total Order</td>
                                        <td class="text-right bold fs-20">$95.00</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right bold fs-20">Shipping</td>
                                        <td class="text-right bold fs-20">$5.00</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right bold fs-20">GST</td>
                                        <td class="text-right bold fs-20">$10.00</td>
                                    </tr>
                                </tbody>
                            </table>

                            <h2 class="bold text-right mr-10">TOTAL: $110.00</h2>
                        </div>
                    </div>
                </div>




                <div class="col-sm-12 col-md-3">
                    <div class="">
                        <a href="/checkout" class="btn btn-lg btn-block btn-green">
                            <i class="flaticon-arrows-5"></i> Download PDF
                        </a>
                    </div>

                    <div class="mt-10">
                        <a href="/checkout" class="btn btn-lg btn-block btn-blue">
                            <i class="flaticon-paper11"></i> Email to someone
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection





@section('js')
    <script>
jQuery(document).ready(function($) {
    $('.table tr[data-href]').each(function(){
        $(this).css('cursor','pointer').hover(
            function(){
                $(this).addClass('active');
            },
            function(){
                $(this).removeClass('active');
            }).click( function(){
                document.location = $(this).attr('data-href');
            }
        );
    });

});
</script>
@endsection
