@extends('front.private')


@section('head')
    <title>My Favourites - Berlish</title>
@endsection


@section('body')
    <section class="products-page">
        <div class="container">
            <h3 class="text-center"><i class="flaticon-like"></i> My Favourites</h3>

            <div class="mt-20">
                <div class="products-list">
                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <a href="">
                                <div class="product">
                                    <div class="extra">
                                        Sale
                                    </div>
                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-shapes"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="/products/103218/grey-boring-shirt-for-winter">
                                            <img src="/assets/img/02.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="/products/103218/grey-boring-shirt-for-winter">
                                            Printed Boohoo Skater Front Tie Dress
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="/products/103218/grey-boring-shirt-for-winter">
                                            $24.80
                                        </a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--/End of product-->

                        <div class="col-lg-3 col-md-4">
                            <a href="">
                                <div class="product">
                                    <div class="extra">
                                        Sale
                                    </div>
                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-shapes"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="/products/103234/too-long-white-dress">
                                            <img src="/assets/img/03.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="/products/103234/too-long-white-dress">
                                            Grey boring shirt for winter
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="/products/103234/too-long-white-dress">
                                            $19.80
                                        </a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--/End of product-->

                        <div class="col-lg-3 col-md-4">
                            <a href="">
                                <div class="product">
                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-shapes"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="">
                                            <img src="/assets/img/04.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="">
                                            Almost Pink Shirt Large Neck
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="">
                                            $14.50
                                        </a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--/End of product-->


                        <div class="col-lg-3 col-md-4">
                            <a href="">
                                <div class="product">

                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-shapes"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="">
                                            <img src="/assets/img/05.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="">
                                            Too Long White Dress
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="">
                                            $49.99
                                        </a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--/End of product-->

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
