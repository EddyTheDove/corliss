@extends('front.private')


@section('head')
    <title>My Account - Berlish</title>
@endsection


@section('body')
    <section class="bag-page">
        <div class="container">
            <h3 class="text-center"><i class="flaticon-social-1"></i> My Account</h3>

            <div class="text-center mt-10">
                <a href="/signout" class="btn btn-default round">Sign Out</a>
            </div>

            <div class="row mt-40">
                <div class="col-sm-6">
                    <div class="bag-card">
                        <div class="card-header">
                            <ul class="list-inline card-options">
                                <li>
                                    <a href=""><i class="flaticon-arrows-1"></i></a>
                                </li>
                            </ul>
                            <h3>Account Details</h3>
                        </div>

                        <form class="form mt-20 pd-10" action="" method="post">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>First name</label>
                                        <input type="text" name="firstname" class="form-control input-lg" value="{{ $user->firstname }}" required>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Last name</label>
                                        <input type="text" name="lastname" class="form-control input-lg" value="{{ $user->lastname }}" required>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control input-lg" value="{{ $user->email }}" required>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" name="phone" class="form-control input-lg" value="{{ $user->phone }}">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" name="address" class="form-control input-lg" value="{{ $user->address }}">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Suburb</label>
                                        <input type="text" name="suburb" class="form-control input-lg" value="{{ $user->suburb }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>State</label>
                                        <div class="category-select grey">
                                            <select name="state">
                                                @foreach($states as $state)
                                                    <option value="{{ $state->code }}" {{ $state->code == $user->state ? 'selected' : '' }}>{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Postcode</label>
                                        <input type="text" name="postcode" class="form-control input-lg" value="{{ $user->postcode }}">
                                    </div>
                                </div>
                            </div>


                            <div class="mt-20 text-center pb-10">
                                <button type="submit" class="btn btn-lg btn-green">
                                    <i class="flaticon-check"></i> Update Account
                                </button>
                            </div>
                        </form>
                    </div>



                    <div class="bag-card">
                        <div class="card-header">
                            <ul class="list-inline card-options">
                                <li>
                                    <a href=""><i class="flaticon-arrows-1"></i></a>
                                </li>
                            </ul>
                            <h3>Change Password</h3>
                        </div>
                    </div>
                    <!--End of billing-->
                </div>


                <div class="col-sm-6">
                    <div class="bag-card">
                        <div class="card-header">
                            <h3>Purchase History</h3>
                        </div>

                        <table class="table table-striped mt-20">
                            <thead>
                                <tr>
                                    <th>Order</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th class="hidden-xs">Date</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr data-href='/orders/11312341'>
                                    <td>11312341</td>
                                    <td>$110.00</td>
                                    <td>Placed</td>
                                    <td class="hidden-xs">11/02/2017</td>
                                </tr>
                                <tr data-href='/orders/11312338'>
                                    <td>11312338</td>
                                    <td>$80.80</td>
                                    <td>Dispatched</td>
                                    <td class="hidden-xs">11/02/2017</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--End of billing-->
                </div>
            </div>
        </div>
    </section>

@endsection




@section('js')
    <script>
jQuery(document).ready(function($) {
    $('.table tr[data-href]').each(function(){
        $(this).css('cursor','pointer').hover(
            function(){
                $(this).addClass('active');
            },
            function(){
                $(this).removeClass('active');
            }).click( function(){
                document.location = $(this).attr('data-href');
            }
        );
    });

});
</script>
@endsection
