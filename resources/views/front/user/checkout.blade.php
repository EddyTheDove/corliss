@extends('front.private')


@section('head')
    <title>Checkout - Berlish</title>
@endsection


@section('body')
    <section class="bag-page">
        <div class="container">
            <h3 class="text-center"><i class="flaticon-shopping-bag"></i> CHECKOUT</h3>

            <div class="row mt-20">
                <div class="col-sm-12 col-md-9">
                    <div class="bag-card">
                        <div class="card-header">
                            <ul class="list-inline card-options">
                                <li>
                                    <a href="/bag"><i class="flaticon-draw"></i> Edit Order</a>
                                </li>
                            </ul>
                            <h3>Order</h3>
                        </div>


                        <table class="table mt-20">
                            <colgroup>
                                <col class="col-xs-8">
                                <col class="col-xs-4">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>1 x Grey boring shirt for winter</td>
                                    <td class="text-right">$24.80</td>
                                </tr>

                                <tr>
                                    <td>1 x Pinky large neck shirt</td>
                                    <td class="text-right">$9.70</td>
                                </tr>

                                <tr>
                                    <td>1 x That cheap cheap shirt</td>
                                    <td class="text-right">$2.00</td>
                                </tr>

                                <tr>
                                    <td>1 x The bitchy face girl</td>
                                    <td class="text-right">$0.99</td>
                                </tr>

                                <tr>
                                    <td class="text-right bold fs-20">TOTAL</td>
                                    <td class="text-right bold fs-20">$95.00</td>
                                </tr>
                            </tbody>
                        </table>


                        <div class="row">
                            <h4 class="ml-30">
                                <a href="" class="bold">I have a promo code</a>
                            </h4>

                            <div class="col-xs-8 col-sm-6">
                                <div class="pd-10 form">
                                    <div class="form-group mt-10">
                                        <input type="text" name="promo" class="input-lg form-control text-center" placeholder="Enter promo code">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-4 col-sm-6">
                                <div class="pd-10 mt-10">
                                    <a href class="btn btn-lg btn-blue">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of order-->

                    <div class="bag-card">
                        <div class="card-header">
                            <ul class="list-inline card-options">
                                <li>
                                    <a href=""><i class="flaticon-arrows-1"></i></a>
                                </li>
                            </ul>
                            <h3>Delivery Address</h3>
                        </div>

                        <div class="form pd-10 mt-10">
                            <form class="" action="" method="post">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text" class="form-control input-lg" value="{{ $user->address }}" name="address" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Suburb</label>
                                            <input type="text" class="form-control input-lg" value="{{ $user->suburb }}" name="suburb" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <div class="category-select grey">
                                                <select name="state">
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->code }}" {{ $state->code == $user->state ? 'selected' : '' }}>{{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Postcode</label>
                                            <input type="text" class="form-control input-lg" value="{{ $user->postcode }}" name="postcode" required>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--End of delivery-->

                    <div class="bag-card">
                        <div class="card-header">
                            <ul class="list-inline card-options">
                                <li>
                                    <a href=""><i class="flaticon-arrows-1"></i></a>
                                </li>
                            </ul>
                            <h3>Billing Address</h3>
                        </div>
                    </div>
                    <!--End of billing-->

                </div>




                <div class="col-sm-12 col-md-3">
                    <div class="bag-checkout">
                        <h3>Summary</h3>

                        <table>
                            <tr>
                                <td>Order</td>
                                <td class="text-right">$95.00</td>
                            </tr>
                            <tr>
                                <td>Shipping</td>
                                <td class="text-right">$5.00</td>
                            </tr>
                            <tr>
                                <td>GST</td>
                                <td class="text-right">$10.00</td>
                            </tr>
                        </table>

                        <div class="total mt-20">
                            <div class="pull-right text-right">
                                $110.00
                            </div>
                            <div class="left">
                                Total + GST
                            </div>
                        </div>

                        <div class="mt-40">
                            <a href="/invoice" class="btn btn-lg btn-block btn-green">
                                Pay by Card
                            </a>
                        </div>

                        <div class="mt-20 text-center">
                            <a href="/invoice">
                                <img src="/assets/img/paypal.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




@endsection
