@extends('front.private')


@section('head')
    <title>Shopping Bag - Berlish</title>
@endsection


@section('body')
    <section class="bag-page">
        <div class="container">
            <h3 class="text-center"><i class="flaticon-shopping-bag"></i> SHOPPING BAG</h3>

            <div class="row mt-20">
                <div class="col-sm-12 col-md-9">
                    <div class="bag-products">
                        <div class="bag-product">
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-lg-2">
                                    <img src="/assets/img/02.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="col-xs-8 col-sm-9 col-lg-10 no-padding">
                                    <div class="content">
                                        <div class="title">Grey boring shirt for winter</div>
                                        <div class="size">Size: M - Quantity: 1 - Colour: Grey</div>
                                        <div class="price">1 x $24.80</div>
                                    </div>

                                    <div class="buttons">
                                        <ul class="list-inline">
                                            <li class="bg-red">
                                                <a href>
                                                    <i class="flaticon-rubbish-bin"></i>
                                                </a>
                                            </li>

                                            <li class="bg-green">
                                                <a href>
                                                    <i class="flaticon-line"></i>
                                                </a>
                                            </li>

                                            <li class="bg-green">
                                                <a href>
                                                    <i class="flaticon-cross"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="bag-product">
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-lg-2">
                                    <img src="/assets/img/04.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="col-xs-8 col-sm-9 col-lg-10 no-padding">
                                    <div class="content">
                                        <div class="title">Pinky large neck shirt</div>
                                        <div class="size">Size: M - Quantity: 1 - Colour: Pink</div>
                                        <div class="price">1 x $9.70</div>
                                    </div>

                                    <div class="buttons">
                                        <ul class="list-inline">
                                            <li class="bg-red">
                                                <a href>
                                                    <i class="flaticon-rubbish-bin"></i>
                                                </a>
                                            </li>

                                            <li class="bg-green">
                                                <a href>
                                                    <i class="flaticon-line"></i>
                                                </a>
                                            </li>

                                            <li class="bg-green">
                                                <a href>
                                                    <i class="flaticon-cross"></i>
                                                </a>
                                            </li>


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="bag-product">
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-lg-2">
                                    <img src="/assets/img/03.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="col-xs-8 col-sm-9 col-lg-10 no-padding">
                                    <div class="content">
                                        <div class="title">That cheap cheap shirt</div>
                                        <div class="size">Size: M - Quantity: 1 - Colour: Pink</div>
                                        <div class="price">1 x $2.00</div>
                                    </div>

                                    <div class="buttons">
                                        <ul class="list-inline">
                                            <li class="bg-red">
                                                <a href>
                                                    <i class="flaticon-rubbish-bin"></i>
                                                </a>
                                            </li>

                                            <li class="bg-green">
                                                <a href>
                                                    <i class="flaticon-line"></i>
                                                </a>
                                            </li>

                                            <li class="bg-green">
                                                <a href>
                                                    <i class="flaticon-cross"></i>
                                                </a>
                                            </li>


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="bag-product">
                            <div class="row">
                                <div class="col-xs-4 col-sm-3 col-lg-2">
                                    <img src="/assets/img/05.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="col-xs-8 col-sm-9 col-lg-10 no-padding">
                                    <div class="content">
                                        <div class="title">The bitchy face girl</div>
                                        <div class="size">Size: M - Quantity: 1 - Colour: White</div>
                                        <div class="price">1 x $0.99</div>
                                    </div>

                                    <div class="buttons">
                                        <ul class="list-inline">
                                            <li class="bg-red">
                                                <a href>
                                                    <i class="flaticon-rubbish-bin"></i>
                                                </a>
                                            </li>

                                            <li class="bg-green">
                                                <a href>
                                                    <i class="flaticon-line"></i>
                                                </a>
                                            </li>

                                            <li class="bg-green">
                                                <a href>
                                                    <i class="flaticon-cross"></i>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>




                <div class="col-sm-12 col-md-3">
                    <div class="bag-checkout">
                        <h3>Summary</h3>

                        <table>
                            <tr>
                                <td>Subtotal</td>
                                <td class="text-right">$95.00</td>
                            </tr>
                            <tr>
                                <td>Shipping</td>
                                <td class="text-right">$5.00</td>
                            </tr>
                            <tr>
                                <td>GST</td>
                                <td class="text-right">$10.00</td>
                            </tr>
                        </table>

                        <div class="total mt-20">
                            <div class="pull-right text-right">
                                $110.00
                            </div>
                            <div class="left">
                                Total + GST
                            </div>
                        </div>

                        <div class="mt-40">
                            <a href="/checkout" class="btn btn-lg btn-block btn-green">
                                <i class="flaticon-supermarket68"></i> CHECKOUT
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




@endsection
