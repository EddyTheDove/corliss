<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <link rel="stylesheet" href="{{ url('assets/css/style.min.css') }}" media="screen" >
    <link rel="stylesheet" href="{{ url('assets/css/app.min.css') }}" media="screen" >
    <meta name="author" content="Corliss Chi, Bernice Choi">
    <meta name="description" content="">
    <meta name="keywords" content="berliss, clothing, ladies cloting, women clothing, second hand clothing">
    @yield('head')
    @include('front.includes.favicon')
</head>
<body ng-app="berlish">

    @include('front.includes.nav')
    @include('front.includes.sidebar')
    @include('front.includes.tabs')
    @yield('body')
    @include('front.includes.footer')



<script src="{{ url('assets/js/scripts.min.js') }}"></script>
<script src="{{ url('assets/js/app.min.js') }}"></script>
<script>
    var __berlish = "<?php echo Auth::user()->api_token; ?>";
</script>
@yield('js')
</body>
</html>
