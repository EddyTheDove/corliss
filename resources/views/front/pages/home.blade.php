@extends('front.body')


@section('head')
    <title>Berlish - Home</title>
@endsection



@section('body')
    @include('front.includes.shipping')
    @include('front.includes.slider')



    <section class="seasonal">
        <div class="container">
            <div class="bg-green">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="seasonal-image">
                            <img src="/assets/design/6987516.jpg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="seasonal-message text-center">
                            <div class="header">
                                Spring Collection
                            </div>
                            <div class="description mt-10">
                                New Spring 2016 Lady's Collection
                            </div>

                            <a href="" class="btn btn-lg btn-white mt">
                                <i class="flaticon-shopping-cart"></i> Shop Collection
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section ng-controller="HomeController">
        <home-products></home-products>
    </section>

@endsection




@section('js')
    <script>
    $('#myCarousel').carousel({
        interval: 5000
    })
    var _prime = <?php echo $prime ?>;
    var _products = <?php echo $products ?>;
    </script>
@endsection
