@extends('front.body')



@section('head')
    <title>Sign In - Berlish</title>
@endsection




@section('body')
    <section class="page">
        <div class="container">
            <div class="row mt-sm mb-40">
            <div class="col-sm-6">
                <h3>Existing Customer</h3>
                <div class="page-card">
                    <form class="form" method="post" action="">
                        {{ csrf_field() }}

                        <p>Welcome back! Please sign in to continue</p>
                        <p>&nbsp;</p>
                        <div class="form-group mb-20">
                            <label>Email</label>
                            <input type="email" class="form-control input-lg square" placeholder="Email" name="email" required>
                        </div>

                        <div class="form-group mb-20">
                            <label>Password</label>
                            <input type="password" class="form-control input-lg square" placeholder="Password" name="password" required>
                        </div>

                        <div class="mt-40 text-center mb-10">
                            <button class="btn btn-green btn-lg w200" type="submit">
                                <i class="icon flaticon-arrows-4"></i> Sign In
                            </button>
                        </div>
                    </form>

                    <div class="text-center fs-16 mt-20">
                        <a href="">
                            <i class="flaticon-padlock27"></i> Forgot password
                        </a>
                    </div>
                </div>

            </div>

            <div class="col-sm-6">
                <h3>New Customer</h3>
                <div class="page-card">
                    <form class="form" method="post" action="/register">
                        {{ csrf_field() }}
                        <p>New to Berliss? Let's build our closet together</p>
                        <p>&nbsp;</p>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control input-lg square mb-20" placeholder="First Name" name="firstname" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control input-lg square mb-20" placeholder="Last Name" name="lastname">
                                </div>
                            </div>
                        </div>

                        <div class="mb-20">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control input-lg square" placeholder="Email" name="email" required>
                            </div>
                        </div>

                        <div class="mt-20 text-center mb-20">
                            <p>By creating an account, you agree to the<a href="terms.html">Terms and Conditions</a> and <a href="/terms">Privacy Policy</a></p>

                            <button class="btn btn-green btn-lg mt-20 w200" type="submit">
                                <i class="icon flaticon-check"></i> Sign Up
                            </button>

                            <p>&nbsp;</p>
                            <h3 class="bold line"><span>OR</span></h3>
                        </div>
                    </form>


                    <div class="text-center pb-20 mt-20">
                        <p>Sign in with Facebook. No-brainer</p>
                        <a href="" class="btn btn-lg btn-blue">
                            <i class="flaticon-facebook2"></i> Sign in with Facebook
                        </a>
                    </div>
                </div>

            </div>
        </div><!--/row-->
        </div>
    </section>
@endsection
