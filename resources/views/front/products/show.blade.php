@extends('front.body')



@section('head')
    <title>Grey boring shirt for winter - Berlish</title>
@endsection





@section('body')
    <section class="product-page">
        <div class="container">
            <div class="row bg-white">
                <div class="col-sm-6 no-padding">
                    <div class="product-left">
                        <div id="main_area">
                            <div class="row">
                                <div class="col-xs-12" id="slider">
                                    <div class="row">
                                        <div class="col-sm-12" id="carousel-bounding-box">
                                            <div class="carousel slide" id="myCarousel">
                                                <div class="carousel-inner">
                                                    <div class="active item" data-slide-number="0">
                                                        <img src="/assets/img/02.jpg">
                                                    </div>
                                                    <div class="item" data-slide-number="1">
                                                        <img src="/assets/img/03.jpg">
                                                    </div>
                                                    <div class=" item" data-slide-number="2">
                                                        <img src="/assets/img/04.jpg">
                                                    </div>
                                                    <div class=" item" data-slide-number="3">
                                                        <img src="/assets/img/05.jpg">
                                                    </div>
                                                </div>

                                                <!-- Carousel nav -->
                                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left flaticon-arrows"></span>
                                                </a>
                                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right flaticon-arrows-3"></span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-sm-4" id="carousel-text"></div>
                                    </div>
                                </div>
                            </div><!--/Slider-->

                            <div class="row " id="slider-thumbs">
                                <ul class="hide-bullets">
                                    <li class="col-xs-3">
                                        <a class="thumbnail" id="carousel-selector-0">
                                            <img src="/assets/img/02.jpg">
                                        </a>
                                    </li>

                                    <li class="col-xs-3">
                                        <a class="thumbnail" id="carousel-selector-1">
                                            <img src="/assets/img/03.jpg">
                                        </a>
                                    </li>

                                    <li class="col-xs-3">
                                        <a class="thumbnail" id="carousel-selector-2">
                                            <img src="/assets/img/04.jpg">
                                        </a>
                                    </li>

                                    <li class="col-xs-3">
                                        <a class="thumbnail" id="carousel-selector-3">
                                            <img src="/assets/img/05.jpg">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End of product image-->


                <div class="col-sm-6">
                    <form class="form product-right">
                        <div class="product-title">
                            <h2>Grey boring shirt for winter</h2>
                        </div>

                        <div class="product-details">
                            <h4>SKU: 103218</h4>
                            <h3>$24.80</h3>
                        </div>

                        <div class="product-specs">
                            <div class="row">
                                <div class="col-sm-8 col-md-6">
                                    <h4>Size</h4>
                                    <div class="sizes">
                                        <ul class="list-inline">
                                            <li>S</li>
                                            <li class="active">M</li>
                                            <li>L</li>
                                            <li>XL</li>
                                        </ul>
                                        <a href="">Sizes guide</a>
                                    </div>
                                </div>
                                <!--End of sizes-->
                            </div>
                            <!--Row-->



                            <div class="row">
                                <div class="col-xs-6">
                                    <h4>Quantity</h4>
                                    <div class="quantity">
                                        <div class="category-select">
                                            <select name="">
                                                <option value="1">1 unit</option>
                                                <option value="2">2 units</option>
                                                <option value="3">3 units</option>
                                                <option value="4">4 units</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!--End of quantity-->

                                <div class="col-xs-6">
                                    <h4>Colour</h4>
                                    <div class="colours">
                                        <div class="category-select">
                                            <select name="">
                                                <option value="1">White</option>
                                                <option value="3">Grey</option>
                                                <option value="2">Navy</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End of colours-->



                            <div class="row mt-20">
                                <div class="col-md-6 col-sm-12">
                                    <div class="">
                                        <button type="submit" class="btn btn-lg btn-green btn-block">
                                            <i class="flaticon-shopping-bag"></i> Add to bag
                                        </button>
                                    </div>

                                    <div class="like mt-10">
                                        <a href="" class="">
                                            <i class="flaticon-like"></i> Add to my treasure
                                        </a>
                                    </div>
                                </div>
                            </div>



                            <div class="mt-40">
                                <h3 class="description">Description <i class="flaticon-arrows-1"></i></h3>

                                <div class="product-description">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                                    ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit
                                    in voluptate velit esse cillum dolore eu fugiat nulla pariatur. E
                                    xcepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                    officia deserunt mollit anim id est laborum.
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--End of product right-->
            </div>
        </div>
    </section>

    <!--Similar items-->
    @include('front.includes.similar')
@endsection





@section('js')
    <script>
    jQuery(document).ready(function($){
        $('#myCarousel').carousel({
            interval: false
        });

        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click(function () {
            var id_selector = $(this).attr("id");
            try {
                var id = /-(\d+)$/.exec(id_selector)[1];
                jQuery('#myCarousel').carousel(parseInt(id));
            } catch (e) {
            }
        });
    })

    </script>
@endsection
