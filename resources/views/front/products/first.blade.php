@extends('front.body')



@section('head')
    <title>Berlish - First Hand</title>
@endsection




@section('body')
    <section class="products-page">
        <div class="container">
            <h2>First Hand</h2>


            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="products-filter">
                        <form class="form" method="get">

                            <h4>Only show</h4>
                            <div class="mt-5">
                                <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                                    <input type="checkbox" name="sub[]" value="dresses"><span></span> Dresses
                                </label>
                            </div>
                            <div class="mt-5">
                                <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                                    <input type="checkbox" name="sub[]" value="skirts"><span></span> Skirts
                                </label>
                            </div>
                            <div class="mt-5">
                                <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                                    <input type="checkbox" name="sub[]" value="tops"><span></span> Tops
                                </label>
                            </div>
                            <div class="mt-5">
                                <label class="css-input css-checkbox css-checkbox-success fs-14 fw-600">
                                    <input type="checkbox" name="sub[]" value="accessories"><span></span> Accessories
                                </label>
                            </div>



                            <h4 class="mt-20">Sort by</h4>
                            <div class="category-select">
                                <select name="">
                                    <option value="cheaper">Price: low to high</option>
                                    <option value="expensive">Price: high to low</option>
                                    <option value="newest">Date: new to old</option>
                                    <option value="oldest">Date: old to new</option>
                                </select>
                            </div>

                            <div class="mt-20">
                                <button type="submit" class="btn btn-lg btn-block btn-green">
                                    <i class="flaticon-controls"></i> Filter
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
                <!--end of sidebar-->



                <div class="col-sm-8">
                    <div class="products-list">
                        <div class="row">
                            <div class="col-lg-4 col-md-6"><a href="">
                                <div class="product">
                                    <div class="extra">
                                        Sale
                                    </div>
                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-like"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="/products/103218/grey-boring-shirt-for-winter">
                                            <img src="/assets/img/02.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="/products/103218/grey-boring-shirt-for-winter">
                                            Printed Boohoo Skater Front Tie Dress
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="/products/103218/grey-boring-shirt-for-winter">
                                            $24.80
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/End of product-->

                            <div class="col-lg-4 col-md-6"><a href="">
                                <div class="product">
                                    <div class="extra">
                                        Sale
                                    </div>
                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-shapes"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="/products/103234/too-long-white-dress">
                                            <img src="/assets/img/03.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="/products/103234/too-long-white-dress">
                                            Grey boring shirt for winter
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="/products/103234/too-long-white-dress">
                                            $19.80
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/End of product-->

                            <div class="col-lg-4 col-md-6"><a href="">
                                <div class="product">
                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-like"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="">
                                            <img src="/assets/img/04.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="">
                                            Almost Pink Shirt Large Neck
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="">
                                            $14.50
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/End of product-->


                            <div class="col-lg-4 col-md-6"><a href="">
                                <div class="product">

                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-shapes"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="">
                                            <img src="/assets/img/05.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="">
                                            Too Long White Dress
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="">
                                            $49.99
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/End of product-->

                            <div class="col-lg-4 col-md-6"><a href="">
                                <div class="product">
                                    <div class="extra">
                                        Sale
                                    </div>
                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-like"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="">
                                            <img src="/assets/img/01.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="">
                                            Red Hair Girl For Sale
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="">
                                            $99.99
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/End of product-->

                            <div class="col-lg-4 col-md-6"><a href="">
                                <div class="product">
                                    <div class="extra">
                                        Sale
                                    </div>
                                    <div class="extra-left">
                                        <a href="#" title="Add to favourites">
                                            <i class="flaticon-like"></i>
                                        </a>
                                    </div>

                                    <div class="product-image">
                                        <a href="">
                                            <img src="/assets/img/06.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="">
                                            Replacement Ear For Sale
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="">
                                            $9.70
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--/End of product-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
