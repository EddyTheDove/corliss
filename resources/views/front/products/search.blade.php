@extends('front.private')


@section('head')
    <title>Search - Berlish</title>
@endsection


@section('body')
    <section class="products-page">
        <div class="container">
            <h2 class="text-center">Search Result</h2>
            <h4 class="text-center">For: {{ Request::get('q') }}</h4>

            <div class="mt-20">
                <div class="products-list">
                    <div class="row">
                        <div class="col-lg-3 col-sm-4">
                            <a href="">
                                <div class="product">
                                    <div class="product-image">
                                        <a href="/products/103218/grey-boring-shirt-for-winter">
                                            <img src="/assets/img/02.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="/products/103218/grey-boring-shirt-for-winter">
                                            Printed Boohoo Skater Front Tie Dress
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="/products/103218/grey-boring-shirt-for-winter">
                                            $24.80
                                        </a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--/End of product-->

                        <div class="col-lg-3 col-sm-4">
                            <a href="">
                                <div class="product">
                                    <div class="product-image">
                                        <a href="/products/103234/too-long-white-dress">
                                            <img src="/assets/img/03.jpg" alt="berlish" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="product-title">
                                        <a href="/products/103234/too-long-white-dress">
                                            Grey boring shirt for winter
                                        </a>
                                    </div>
                                    <div class="product-price">
                                        <a href="/products/103234/too-long-white-dress">
                                            $19.80
                                        </a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--/End of product-->

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
