<!DOCTYPE html>
<html class="no-focus">
<head>
<meta charset="utf-8">
<title>Berlis - Admin</title>
<meta name="description" content="Berlis Admin">
<meta name="author" content="Eddy Ella">
<meta name="robots" content="noindex, nofollow">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
<link rel="apple-touch-icon" sizes="57x57" href="/backend/img/favicons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/backend/img/favicons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/backend/img/favicons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/backend/img/favicons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/backend/img/favicons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/backend/img/favicons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/backend/img/favicons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/backend/img/favicons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/backend/img/favicons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/backend/img/favicons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/backend/img/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/backend/img/favicons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/backend/img/favicons/favicon-16x16.png">
<meta name="msapplication-TileImage" content="/backend/img/favicons/ms-icon-144x144.png">
<link rel="shortcut icon" href="/backend/img/favicons/favicon.png">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
<link rel="stylesheet" id="css-main" href="/backend/css/berlis.css">
<link rel="stylesheet" id="css-main" href="/assets/css/eddy.css">
</head>
<body>
<div class="content overflow-hidden mt-40">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
            
            @include('errors.list')


            <div class="block block-themed animated fadeIn">
                <div class="block-content block-content-full block-content-narrow">
                    

                    <h1 class="h2 font-w600 mb-40 text-center">Berlis</h1>

                    <form class="js-validation-login form-horizontal" action="" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-material form-material-primary floating">
                                    <input class="form-control" type="email" id="login-email" name="email" value="{{ old('email') }}">
                                    <label for="login-email">Email</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-material form-material-primary floating">
                                    <input class="form-control" type="password" id="login-password" name="password">
                                    <label for="login-password">Password</label>
                                </div>
                            </div>
                        </div>


                        <div class="text-center mt-40 mb-40">
                            <button class="btn btn-blue btn-lg" type="submit">
                            	 Sign in <i class="si si-login ml-10"></i>
                            </button>
                        </div>
                    </form>

					<div class="text-center">
						<a href=""><i class="si si-lock"></i> Forgot Password?</a>	
					</div>
                    
                </div>
            </div>
        </div>
    </div>
</div>



<div class="push-10-t text-center animated fadeInUp">
	<small class="text-muted font-w600">&copy; Berlis {{ date('Y') }}</small>
</div>
<script src="/backend/js/core/jquery.min.js"></script>
<script src="/backend/js/core/bootstrap.min.js"></script>
<script src="/backend/js/core/jquery.slimscroll.min.js"></script>
<script src="/backend/js/core/jquery.scrollLock.min.js"></script>
<script src="/backend/js/core/jquery.appear.min.js"></script>
<script src="/backend/js/core/jquery.countTo.min.js"></script>
<script src="/backend/js/core/jquery.placeholder.min.js"></script>
<script src="/backend/js/core/js.cookie.min.js"></script>
<script src="/backend/js/app.js"></script>
<script src="/backend/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="/backend/js/pages/base_pages_login.js"></script>
</body>