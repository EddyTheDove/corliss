@extends('one.body')
@section('head')
<link rel="stylesheet" type="text/css" href="/assets/fancybox/jquery.fancybox.css" media="screen" />
@endsection

@section('content')
<section class="content">
    <div class="row">
        <h4 class="line line-grey mb-40"><span>{{ $product->name }}</span></h4>
        <div class="col-sm-12">
            @include('errors.list')
        </div>


        <div class="col-sm-12">
            <div class="block">
                <ul class="nav nav-tabs" data-toggle="tabs">
                    <li class="active">
                        <a href="#product">Product</a>
                    </li>
                    <li>
                        <a href="#items">Items</a>
                    </li>
                    <li>
                        <a href="#gallery">Gallery</a>
                    </li>
                </ul>



                <div class="block-content tab-content">
                    <div class="tab-pane active" id="product">
                        <div class="row">
                            <div class="col-sm-3 mb-20">
                                <img src="{{ $product->image }}" class="img-responsive">
                            </div>
                            <div class="col-sm-9 no-padding-left">
                                <a class="btn btn-red mb-10" href="/one/products/{{ $product->sku }}/edit">
                                    <i class="si si-pencil"></i> Edit
                                </a>

                                <ul class="list-unstyled fs16 lh-25">
                                    <li><span class="text-muted">SKU:</span> {{ $product->sku }}</li>
                                    <li><span class="text-muted">Status:</span> {{ $product->status ? 'Public' : 'Private' }}
                                    <li><span class="text-muted">Sale:</span> {{ $product->sale > 0 ? '$' . $product->sale : '--' }}</li>
                                    <li><span class="text-muted">Price:</span> ${{ $product->price }}</li>
                                    <li><span class="text-muted">Stock:</span> {{ $product->in_stock ? 'In Stock' : 'Out of stock' }}</li>
                                    <li><span class="text-muted">Quantity:</span> {{ $product->quantity ? $product->quantity : 'No item' }}</li>
                                    <li><span class="text-muted">GST:</span> {{ $product->has_gst ? 'Yes' : 'No' }}</li>
                                    <li><span class="text-muted">Max order:</span> {{ $product->max_order }}</li>
                                    <li><span class="text-muted">Added:</span> {{ date('d M Y @ H:i', strtotime($product->created_at)) }}</li>
                                    <li><span class="text-muted">Edited:</span> {{ date('d M Y @ H:i', strtotime($product->updated_at)) }}</li>
                                </ul>
                            </div>
                        </div>

                        <div>
                            {!! $product->description !!}
                        </div>
                    </div>







                    <div class="tab-pane" id="items">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="block mt-20">
                                    <div class="block-content pb-20">
                                        <form method="post" action="/one/items">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                                            <input type="hidden" class="form-control" id='profile' name='image' value="{{ old('input') }}">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Colour</label>
                                                        <select class="form-control" name="colour_id">
                                                            @foreach($colours as $colour)
                                                            <option value="{{ $colour->id }}">{{ $colour->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Size</label>
                                                        <select class="form-control" name="size_id">
                                                            @foreach($sizes as $size)
                                                            <option value="{{ $size->id }}">{{ $size->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Price</label>
                                                        <input type="text" class="form-control" name="price" value="{{ $product->price }}">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Sale</label>
                                                        <input type="text" class="form-control" name="sale" value="{{ $product->sale }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Display Rank</label>
                                                        <input type="text" class="form-control" name="rank" value="1">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Quantity</label>
                                                        <input type="text" class="form-control" name="quantity" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            

                                            <div class="text-right">
                                                <a href="/backend/filemanager/dialog.php?type=1&field_id=profile" class="iframe-btn btn-white btn pull-left"> <i class='fa fa-folder-open'></i> Files</a>    
                                            

                                                <button class="btn btn-blue">
                                                    <i class="fa fa-floppy-o"></i> Save
                                                </button>
                                            </div>

                                            <div id="profile_view" class="mt-20"></div>
                                        </form>  
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                @foreach($product->items as $item)
                                    <div class="row mb-20">
                                        <div class="col-sm-12">
                                            <h4 class="block-title mb-10">
                                                <a href="/one/items/{{ $item->id }}/edit" class="dark">
                                                    <i class="fa fa-pencil"></i>  
                                                    {{ $item->colour->name }} - {{ $item->size->name }}
                                                </a>
                                            </h4>
                                        </div>

                                        <div class="col-sm-3">
                                            <img src="{{ $item->thumb() }}" class="img-responsive">
                                        </div>
                                        <div class="col-sm-9 no-padding-left">
                                            <ul class="list-unstyled">
                                                @if($item->sale > 0)
                                                <li>${{ $item->sale }} <span class="line-through">${{ $item->price }}</span></li>
                                                @else
                                                <li>${{ $item->price }}</li>
                                                @endif
                                                <li>{{ $item->quantity }} in stock</li>
                                                <li>{{ date('d M Y @ H:i', strtotime($item->created_at)) }}</li>
                                                <li>{{ date('d M Y @ H:i', strtotime($item->updated_at)) }}</li>
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div><!--/items tab-->






                    <div class="tab-pane" id="gallery">
                        <div class="row">
                            <div class="col-sm-6">
                                <form method="post" action="/one/galleries">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">

                                    <div class="row">
                                        <label>Rank</label>
                                        <div class="col-xs-3 mb-10">
                                            <input type="text" name="rank" class="form-control" value="1" numeric>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 mb-10">
                                            <label>Link</label>
                                            <input type="text" name="gallery" id="gallery_image" class="form-control">
                                        </div>
                                    </div>
                                    


                                    <div class="text-right">
                                        <a href="/backend/filemanager/dialog.php?type=1&field_id=gallery_image" class="iframe-btn btn-white btn pull-left"> <i class='fa fa-folder-open'></i> Files</a>    
                                    

                                        <button class="btn btn-blue">
                                            <i class="fa fa-floppy-o"></i> Save
                                        </button>
                                    </div>

                                    <div id="gallery_view" class="mt-20"></div>
                                </form>
                            </div>


                            <div class="col-sm-6">
                                @if($product->hasGallery())
                                    <div class="row">
                                    @foreach($product->gallery as $picture)
                                        <div class="col-sm-4 mb-10">
                                            <div class="text-center">
                                                <a href="/one/galleries/{{ $picture->id }}/delete" class="red" class="absolute">
                                                    <i class="fa fa-trash"></i> Delete
                                                </a>
                                            </div>
                                            <img src="{{ $picture->thumb() }}" class="thumbnail img-responsive">
                                        </div>
                                    @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                        
                    </div><!--gallery tab-->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection



@section('js')
<script type="text/javascript" src="/assets/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript">
    $('.iframe-btn').fancybox({ 
    'width'     : 900,
    'maxHeight' : 600,
    'minHeight'    : 400,
    'type'      : 'iframe',
    'autoSize'      : false
    });

    $("body").hover(function() {
        var gallery = $("input[name='gallery']").val();
        var profilePic = $("input[name='image']").val();
        if(profilePic)
            $('#profile_view').html("<img class='img-responsive mb-10' src='" + profilePic +"'/>");

        if(gallery)
            $('#gallery_view').html("<img class='img-responsive mb-10' src='" + gallery +"'/>");
    });
</script>
@stop