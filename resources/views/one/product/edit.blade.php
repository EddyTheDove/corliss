@extends('one.body')
@section('head')
<link rel="stylesheet" type="text/css" href="/assets/fancybox/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" href="/backend/js/plugins/jquery-tags-input/jquery.tagsinput.min.css">
@endsection

@section('content')



<section class="content mt-10">
    <h4 class="line line-grey">
        <span>{{ $product->name }}</span>
    </h4>

{!! Form::model($product, ['method' => 'PATCH', 'action' => ['Views\Back\ProductController@update', $product->id]]) !!}
    <div class="row">
        <div class="col-sm-12">
            @include('errors.list')
        </div>

        <div class="col-sm-12 text-right mb-20">
            <a class="btn btn-white pull-left" href="/one/products/{{ $product->sku }}">
                <i class="si si-action-undo"></i> Product
            </a>
            <button class="btn btn-blue" type="submit">
                <i class="fa fa-floppy-o"></i> Update
            </button>
        </div>

        <div class="col-sm-7 col-md-8">
            <div class="block block-bordered">
                <div class="block-header">
                    <div class="h5">Product details</div>
                </div>

                <div class="block-content pb-20">
                    <div class="form-group mb-20">
                        <label>Product Name</label>
                        <input type="text" class="form-control input-lg" name="name" placeholder="Product name" value="{{ $product->name }}" id="slug-source">
                    </div>


                    <div class="form-group mb-20">
                        <label>Product URL</label>
                        <input type="text" class="form-control input-lg" name="slug" placeholder="Product URL" value="{{ $product->slug }}" id="slug-target">
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Product Status</label>
                                <select class="form-control input-lg" name="status">
                                    <option value="0" {{ !$product->status ? 'selected' : ''}}>Private</option>
                                    <option value="1" {{ $product->status ? 'selected' : ''}}>Public</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Stock Control</label>
                                <select class="form-control input-lg" name="in_stock">
                                    <option value="0" {{ !$product->in_stock ? 'selected' : ''}}>Out of stock</option>
                                    <option value="1" {{ $product->in_stock ? 'selected' : ''}}>In stock</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-20">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Has GST?</label>
                                <select class="form-control input-lg" name="has_gst">
                                    <option value="0">No</option>
                                    <option value="1" {{ $product->has_gst ? 'selected' : ''}}>Yes</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Max Order Qty</label>
                                <select class="form-control input-lg" name="max_order">
                                    <option value="0">Unlimited</option>
                                    @for($i = 1; $i <= 10; $i++)
                                        <option value="{{ $i }}" {{ $product->max_order == $i ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="form-group mb-20">
                        <label>Tags</label>
                        <div class="">
                            <input class="js-tags-input form-control input-lg" type="text" id="example-tags1" name="tags" value="{{ $product->tags }}">
                        </div>
                    </div>



                    <div class="row mb-4 0">
                        <div class="col-sm-6">
                            <div class="form-group mb-10">
                                <label>Price</label>
                                <input type="text" class="form-control input-lg" name="price" placeholder="Default price" value="{{ $product->price }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group mb-10">
                                <label>Sale</label>
                                <input type="text" class="form-control input-lg" name="sale" placeholder="Sale price"value="{{ $product->sale }}">
                            </div>
                        </div>
                    </div>

                    <label>Product description</label>
                    <textarea class="form-control tiny" name="description">{{ $product->description }}</textarea>
                </div>
            </div>
        </div>


        <div class="col-sm-5 col-md-4">
            <div class="block block-bordered">
                <div class="block-header">
                    <div class="h5">Categories</div>
                </div>

                <div class="block-content" style="max-height: 300px;overflow: scroll;">
                    <ul class="list-unstyled">
                        @foreach($categories as $cat)
                            <li>
                                <label class="css-input css-checkbox css-checkbox-primary">
                                    <input type="checkbox" name="categories[]" value="{{ $cat->id }}"
                                    @foreach($product->categories as $pc)
                                        {{ $pc->id == $cat->id ? 'checked' : '' }}
                                    @endforeach
                                    >
                                    <span></span> {{ $cat->name }}
                                </label>
                            </li>
                            @foreach($cat->children as $child)
                                <li class="ml-20">
                                    <label class="css-input css-checkbox css-checkbox-primary">
                                        <input type="checkbox" name="categories[]" value="{{ $child->id }}"
                                        @foreach($product->categories as $pc)
                                            {{ $pc->id == $child->id ? 'checked' : '' }}
                                        @endforeach
                                        >
                                        <span></span> {{ $child->name }}
                                    </label>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            </div>


            <div class="block block-bordered">
                <div class="block-header">
                    <div class="h5">Preview Image</div>
                </div>

                <div class="block-content pb-10">
                    <input type="hidden" class="form-control" id='profile' name='image' readonly value="{{ $product->image }}">
                    <div id="profile_view"></div>

                    <div class="text-right">
                        <a href="/backend/filemanager/dialog.php?type=1&field_id=profile" class="iframe-btn btn-white btn "> <i class='fa fa-folder-open'></i> Files</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-sm-12 text-right mt-20 mb-20">
            <button class="btn btn-blue" type="submit">
                <i class="fa fa-floppy-o"></i> Update
            </button>
        </div>


    </div>
{!! Form::close() !!}
</section>
@endsection



@section('js')
<script type="text/javascript" src="/assets/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="/backend/tinymce/tinymce.min.js"></script>
<script src="/backend/js/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="/backend/js/plugins/slugify/speakingurl.min.js"></script>
<script type="text/javascript" src="/backend/js/plugins/slugify/slug.min.js"></script>
<script type="text/javascript">
    $('.iframe-btn').fancybox({
    'width'     : 900,
    'maxHeight' : 600,
    'minHeight'    : 400,
    'type'      : 'iframe',
    'autoSize'      : false
    });

    $("body").hover(function() {
        var profilePic = $("input[name='image']").val();
        if(profilePic)
            $('#profile_view').html("<img class='thumbnail img-responsive mb-10' src='" + profilePic +"'/>");
    });

    $('#slug-target').slugify('#slug-source');

    tinymce.init({
        selector: ".tiny",
        theme: "modern",
        relative_urls: false,
        height : 280,
        theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
        font_size_style_values : "10px,12px,13px,14px,16px,18px,20px",
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak",
             "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
             "table contextmenu directionality emoticons paste textcolor filemanager code"
       ],
       toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
       toolbar2: "|filemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
       image_advtab: true ,

        external_filemanager_path:"/backend/filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "/backend/filemanager/plugin.min.js"}
    });

    $(function () {
        App.initHelpers(['tags-inputs']);
    });
</script>
@endsection
