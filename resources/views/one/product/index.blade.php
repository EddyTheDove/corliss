@extends('one.body')


@section('content')
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Products <small>For Products & Variations</small>
            </h1>
        </div>
    </div>
</div>

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            @include('errors.list')

			<div class="text-right">
				<a href="/one/products/create" class="btn btn-blue ">
					<i class="fa fa-plus"></i> Add Product
				</a>
			</div>
            <div class="block block-bordered mt-20">
                <div class="block-content pb-20">
                    <table class="table table-striped">
                        <col class="col-xs-2">
                        <col class="col-xs-8">
                        <col class="col-xs-2">

                        <thead>
                            <tr>
                                <th>SKU</th>
                                <th>Name</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody class="fs16 rowlink">
                            @foreach($products as $product)
                                <tr data-href="/one/products/{{ $product->sku }}">
                                    <td class="bold fw-600">{{ $product->sku }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td><i class="si {{ $product->status ? 'si-like' : 'si-dislike' }}"></i></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
    </div>
</section>
@endsection



@section('js')
<script>
$(function(){
    $('.table tr[data-href]').each(function(){
        $(this).css('cursor','pointer').hover(
            function(){ 
                $(this).addClass('active'); 
            },  
            function(){ 
                $(this).removeClass('active'); 
            }).click( function(){ 
                document.location = $(this).attr('data-href'); 
            }
        );
    });
});
</script>
@stop