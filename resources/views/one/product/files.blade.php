@extends('one.body')


@section('content')
<section class="content">
    <h3 class="line line-grey mb-40"><span>Files</span></h3>

    <div class="block">
        <div class="block-content">
            <iframe  width="100%" height="600" frameborder="0"
                src="/backend/filemanager/dialog.php?type=0">
            </iframe>
        </div>
    </div>
</section>
@endsection
