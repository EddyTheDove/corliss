@extends('one.body')
@section('head')
<link rel="stylesheet" type="text/css" href="/assets/fancybox/jquery.fancybox.css" media="screen" />
@endsection

@section('content')
<section class="content">
    <div class="row">
        <h4 class="line line-grey mb-40"><span>{{ $item->product->name }}</span></h4>
        <div class="col-sm-12">
            @include('errors.list')
        </div>

        <div class="col-sm-12">
            <a class="btn btn-white pull-left" href="/one/products/{{ $item->product->sku }}">
                <i class="si si-action-undo"></i> Product
            </a>
        </div>


        <div class="col-sm-8 mb-40">
            {!! Form::model($item, ['method' => 'PATCH', 'action' => ['Views\Back\ItemController@update', $item->id]]) !!}
            
            <input type="hidden" class="form-control" id='profile' name='image' value="{{ $item->image }}">
            
            <div class="block mt-20">
                <div class="block-content pb-20">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Colour</label>
                                <select class="form-control" name="colour_id">
                                    @foreach($colours as $colour)
                                    <option value="{{ $colour->id }}" {{ $colour->id == $item->colour_id ? 'selected' : ''  }}>
                                        {{ $colour->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Size</label>
                                <select class="form-control" name="size_id">
                                    @foreach($sizes as $size)
                                    <option value="{{ $size->id }}" {{ $size->id == $item->size_id ? 'selected' : '' }}>
                                        {{ $size->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" class="form-control" name="price" value="{{ $item->price }}">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Sale</label>
                                <input type="text" class="form-control" name="sale" value="{{ $item->sale }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Display Rank</label>
                                <input type="text" class="form-control" name="rank" value="{{ $item->rank }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Quantity</label>
                                <input type="text" class="form-control" readonly value="{{ $item->quantity }}">
                            </div>
                        </div>
                    </div>
                            
                            
                            

                    <div class="text-right">
                        <a href="/backend/filemanager/dialog.php?type=1&field_id=profile" class="iframe-btn btn-white btn pull-left"> <i class='fa fa-folder-open'></i> Files</a>    
                    

                        <button class="btn btn-blue">
                            <i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>


        <div class="col-sm-4 mb-40">
            <div class="block block-bordered mt-20">
                <div class="block-header">
                    <div class="h5">Stock Control</div>
                </div>
                <div class="block-content">
                    <form method="post" action="/one/items/stock">
                        {{ csrf_field() }}
                        <input type="hidden" name="item_id" value="{{ $item->id }}">
                        <label>Increase by</label>
                        <input type="text" class="form-control" name="quantity" required placeholder="Quantity">
                        <small>Enter negative numbers to decrease stock</small>

                        <div class="text-right mt-10 mb-20">
                            <button class="btn btn-red">
                                <i class="si si-refresh"></i> Update Stock
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="block mt-20">
                <div class="block-content">
                    <div id="profile_view" class="mb-20"></div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection



@section('js')
<script type="text/javascript" src="/assets/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript">
    $('.iframe-btn').fancybox({ 
    'width'     : 900,
    'maxHeight' : 600,
    'minHeight'    : 400,
    'type'      : 'iframe',
    'autoSize'      : false
    });

    $("body").hover(function() {
        var profilePic = $("input[name='image']").val();
        if(profilePic)
            $('#profile_view').html("<img class='img-responsive mb-10' src='" + profilePic +"'/>");
    });

    $('#form').hide();
    $('#showform').on('click', function(){
        $('#form').slideToggle('fast');
    });
</script>
@stop