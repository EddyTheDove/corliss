@extends('one.body')


@section('content')
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Orders 
            </h1>
        </div>
    </div>
</div>

<section class="content mt-40">
    <div class="row">
        <div class="col-sm-12">
            @include('errors.list')
        </div>
    </div>
</section>
@endsection