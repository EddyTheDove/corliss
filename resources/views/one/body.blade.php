@include('one.includes.head')
<body>  

	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
		<div class="loader">
	      <div class="fading-line"></div>
	    </div>

		@include('one.includes.sidebar')
        @include('one.includes.header')

		 <main id="main-container">
            @yield('content')
        </main>
        @include('one.includes.footer')
	</div>

@include('one.includes.js')
</body>
</html>