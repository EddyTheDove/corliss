@extends('one.body')


@section('content')
<section class="content mt-40">
    <div class="row">
        <div class="col-sm-12">
            @include('errors.list')
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="block">
                <table class="block-table text-center">
                    <tbody>
                        <tr>
                            <td class="bg-primary-darker" style="width: 50%;">
                                <div class="push-30 push-30-t">
                                    <i class="si si-handbag fa-3x text-white-op"></i>
                                </div>
                            </td>
                            <td style="width: 50%;">
                                <div class="h1 font-w700">{{ $orders }}</div>
                                <div class="h5 text-muted text-uppercase push-5-t">Order{{ $orders > 1 ? 's' : '' }}</div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="block">
                <table class="block-table text-center">
                    <tbody>
                        <tr>
                            <td class="bg-success" style="width: 50%;">
                                <div class="push-30 push-30-t">
                                    <i class="si si-present fa-3x text-white-op"></i>
                                </div>
                            </td>
                            <td style="width: 50%;">
                                <div class="h1 font-w700">{{ $deliveries }}</div>
                                <div class="h5 text-muted text-uppercase push-5-t">Deliver{{ $deliveries > 1 ? 'ies' : 'y' }}</div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="block">
                <table class="block-table text-center">
                    <tbody>
                        <tr>
                            <td style="width: 50%;">
                                <div class="h1 font-w700">{{ $products }}</div>
                                <div class="h5 text-muted text-uppercase push-5-t">Products</div>
                            </td>
                            <td class="bg-success" style="width: 50%;">
                                <div class="push-30 push-30-t">
                                    <i class="si si-basket-loaded fa-3x text-white-op"></i>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="block">
                <table class="block-table text-center">
                    <tbody>
                        <tr>
                            <td style="width: 50%;">
                                <div class="h1 font-w700">{{ $users }}</div>
                                <div class="h5 text-muted text-uppercase push-5-t">Users</div>
                            </td>
                            <td class="bg-primary-darker" style="width: 50%;">
                                <div class="push-30 push-30-t">
                                    <i class="si si-users fa-3x text-white-op"></i>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@endsection