@extends('one.body')
@section('head')
<link rel="stylesheet" href="/backend/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
@endsection

@section('content')
<section class="content">
    <h3 class="line line-grey mb-40"><span>Variations</span></h3>

	<div class="row">
		<div class="col-sm-12">
			@include('errors.list')
		</div>


		<div class="col-sm-6">
			<div class="block block-bordered">
				<div class="block-header">
					<ul class="block-options">
                        <li>
                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                        </li>
                    </ul>
					<h3 class="block-title">Colours</h3>
				</div>

				<div class="block-content">
                    <form class="mb-20" method="post" action="/one/variations/colours">
                    	{{ csrf_field() }}
                    	<div class="row">
                    		<div class="col-sm-6">
                    			<label>Colour Code</label>
                    			<div class="form-group">
		                    		<div class="js-colorpicker input-group">
			                            <input class="form-control" type="text" name="value" value="#000000" required>
			                            <span class="input-group-addon"><i></i></span>
			                        </div>
		                    	</div>
                    		</div>
                    		<div class="col-sm-6">
                    			<label>Colour name</label>
                    			<div class="form-group">
		                    		<div class="">
			                            <input class="form-control" type="text" name="name" required placeholder="Colour Name">
			                        </div>
		                    	</div>
                    		</div>
                    	</div>
                    	
                    	<div class="text-right">
                    		<button class="btn btn-blue">
	                    		<i class="fa fa-floppy-o"></i> Save
	                    	</button>
                    	</div>
                    	
                    </form>
                        

					<table class="table table-striped">
						<col class="col-xs-1">
						<col class="col-xs-1">
						<col class="col-xs-10">
						<tbody>
							@foreach($colours as $colour)
							<tr>
								<td>
									<a href="/one/variations/colours/{{ $colour->id }}/delete">
										<i class="fa fa-trash-o fa-2x dark"></i>
									</a>
								</td>
								<td>
									<ul class="list-unstyled">
										<li class="single-color" style="background-color: {{ $colour->value }};"> 
										</li>
									</ul>
								</td>
								<td> {{ $colour->name }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>


		<div class="col-sm-6">
			<div class="block block-bordered">
				<div class="block-header">
					<ul class="block-options">
                        <li>
                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                        </li>
                    </ul>
					<h3 class="block-title">Sizes</h3>
				</div>

				<div class="block-content">
					<form class="mb-20" method="post" action="/one/variations/sizes">
                    	{{ csrf_field() }}
                    	<div class="row">
                    		<div class="col-sm-6">
                    			<label>Size code</label>
                    			<div class="form-group">
		                    		<div class="">
			                            <input class="form-control" type="text" name="short" required placeholder="Size short">
			                        </div>
		                    	</div>
                    		</div>
                    		<div class="col-sm-6">
                    			<label>Size name</label>
                    			<div class="form-group">
		                    		<div class="">
			                            <input class="form-control" type="text" name="name" required placeholder="Size name">
			                        </div>
		                    	</div>
                    		</div>
                    	</div>
                    	
                    	<div class="text-right">
                    		<button class="btn btn-blue">
	                    		<i class="fa fa-floppy-o"></i> Save
	                    	</button>
                    	</div>
                    	
                    </form>

	
					<table class="table table-striped">
						<col class="col-xs-1">
						<col class="col-xs-1">
						<col class="col-xs-10">
						<tbody>
							@foreach($sizes as $size)
							<tr>
								<td>
									<a href="/one/variations/sizes/{{ $size->id }}/delete">
										<i class="fa fa-trash-o fa-2x dark"></i>
									</a>
								</td>
								<td>{{ $size->short }}</td>
								<td> {{ $size->name }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection


@section('js')
<script src="/backend/js/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">
$(function () {
    App.initHelpers(['colorpicker']);
});
</script>

@endsection