@extends('one.body')


@section('content')
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                {{ $category->name }}
            </h1>
        </div>
    </div>
</div>

<section class="container mt-40">
    <div class="row">
        
         <div class="col-sm-8 col-sm-offset-2">
            <div class="mt-10 mb-20">
                <a href="/one/categories" class="btn btn-lg btn-white">
                    <i class="si si-action-undo"></i> Back
                </a>
            </div>


            <div class="block block-bordered">
                <div class="block-header text-center">
                    <h4>Category details</h4>
                </div>

                <div class="block-content pb-20">
                    @include('errors.list')

                    {!! Form::model($category, ['method' => 'PATCH', 'action' => ['Views\Back\CategoryController@update', $category->id]]) !!}
                       

                        <div class="form-group mt-20">
                            <label class="fs16">Category name</label>
                            <input type="text" class="form-control input-lg fs16" name="name" placeholder="Category name" id="name" value="{{ $category->name }}">
                        </div>
                        <div class="form-group mt-20">
                            <label class="fs16">Slug</label>
                            <input type="text" class="form-control input-lg fs16" name="slug" placeholder="Slug" id="slug" value="{{ $category->slug }}">
                        </div>

                        <label for="">Parent</label>
                        <select name="parent" id="" class="form-control input-lg">
                            <option value="">None</option>
                            @foreach($categories as $cat)
                                <option value="{{ $cat->id }}" {{ $cat->id == $category->parent ? 'selected' : '' }}>{{ $cat->name }}</option>
                            @endforeach
                        </select>


                        <label class="mt-20">Description</label>
                        <textarea name="description" rows="3" class="form-control">{{ old('description') }}</textarea>


                        <div class="text-right mt-20">
                            <button class="btn btn-lg btn-blue">
                                <i class="fa fa-floppy-o mr-10"></i> Save
                            </button>
                        </div>
                    {!! Form::close() !!}


                    <div class="mt-40 text-center">
                        {!! Form::model($category, ['method' => 'DELETE', 'action' => ['Views\Back\CategoryController@destroy', $category->id]]) !!}
                            @if(count($category->products))
                                <p class="fs16">You cannot delete a category that contains products</p>
                            @endif
                            <button class="btn btn-default {{ count($category->products)  ? 'disabled' : '' }}">
                                <i class="fa fa-trash-o"></i> Delete
                            </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
         </div>                
    </div>
</section>
@stop


@section('js')
<script src="/backend/js/plugins/slugify/jquery.slugify.js"></script>
<script>
$('#slug').slugify('#name');

</script>
@stop