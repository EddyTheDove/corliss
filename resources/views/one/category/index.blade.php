@extends('one.body')


@section('content')
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Categories <small>For Product categories & Sub Categories</small>
            </h1>
        </div>
    </div>
</div>

<section class="container mt-40">
    <div class="row">
         <div class="col-md-6 col-lg-4">
            <div class="block block-bordered">
                <div class="block-header text-center">
                    <h4>Category details</h4>
                </div>

                <div class="block-content pb-20">
                    @include('errors.list')

                    <form action="/one/categories" method="post">
                        {{ csrf_field() }}

                        <div class="form-group mt-20">
                            <label class="fs16">Category name</label>
                            <input type="text" class="form-control input-lg fs16" name="name" placeholder="Category name" id="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group mt-20">
                            <label class="fs16">Slug</label>
                            <input type="text" class="form-control input-lg fs16" name="slug" placeholder="Slug" id="slug" value="{{ old('slug') }}">
                        </div>

                        <label for="">Parent</label>
                        <select name="parent" id="" class="form-control input-lg">
                            <option value="">None</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>


                        <label class="mt-20">Description</label>
                        <textarea name="description" rows="2" class="form-control">{{ old('description') }}</textarea>


                        <div class="text-right mt-20">
                            <button class="btn btn-lg btn-blue">
                                <i class="fa fa-floppy-o mr-10"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
         </div>


         <div class="col-md-6 col-lg-8">
             <div class="block block-bordered">
                <div class="block-header text-center">
                    <h4>All Categories</h4>
                </div>

                <div class="block-content pb-20">
                    <table class="table table-striped">
                        <col class="col-xs-5">
                        <col class="col-xs-4">
                        <col class="col-xs-3">

                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Products</th>
                            </tr>
                        </thead>
                        <tbody class="fs16 rowlink">
                            @foreach($categories as $category)
                                <tr data-href="/one/categories/{{ $category->id }}/edit">
                                    <td class="bold fw-600">{{ $category->name }}</td>
                                    <td>{{ $category->slug }}</td>
                                    <td>{{ number_format(count($category->products)) }}</td>
                                </tr>
                                @if($category->children)
                                    @foreach($category->children as $child)
                                        <tr data-href="/one/categories/{{ $child->id }}/edit">
                                            <td><span class="mr-20"></span>{{ $child->name }}</td>
                                            <td>{{ $child->slug }}</td>
                                            <td>{{ number_format(count($child->products)) }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
         </div>
    </div>
</section>
@stop


@section('js')
<script src="/backend/js/plugins/slugify/jquery.slugify.js"></script>
<script>
$('#slug').slugify('#name');

$(function(){
    $('.table tr[data-href]').each(function(){
        $(this).css('cursor','pointer').hover(
            function(){
                $(this).addClass('active');
            },
            function(){
                $(this).removeClass('active');
            }).click( function(){
                document.location = $(this).attr('data-href');
            }
        );
    });
});
</script>
@stop
