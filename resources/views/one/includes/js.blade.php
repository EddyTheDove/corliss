<script src="/backend/js/core/jquery.min.js"></script>
<script src="/backend/js/core/bootstrap.min.js"></script>
<script src="/backend/js/core/jquery.slimscroll.min.js"></script>
<script src="/backend/js/core/jquery.scrollLock.min.js"></script>
<script src="/backend/js/core/jquery.appear.min.js"></script>
<script src="/backend/js/core/jquery.countTo.min.js"></script>
<script src="/backend/js/core/jquery.placeholder.min.js"></script>
<script src="/backend/js/core/js.cookie.min.js"></script>
<script src="/backend/js/app.js"></script>
<script src="/backend/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="/backend/js/pages/base_pages_login.js"></script>
<script type="text/javascript">
$(".alert-success").fadeTo(5000, 500).slideUp(500, function(){
    $(".alert-success").alert('close');
});
$(window).load(function () {
    $(".loader .fading-line").fadeOut();
    $(".loader").fadeOut("slow");
});
</script>
@yield('js')