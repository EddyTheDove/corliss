<!DOCTYPE html>
<html class="no-focus">
<head>
<meta charset="utf-8">
@yield('head')
<title>Berlis - Admin</title>
<meta name="description" content="Berlis Admin">
<meta name="author" content="Eddy Ella">
<meta name="robots" content="noindex, nofollow">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
<link rel="apple-touch-icon" sizes="57x57" href="/backend/img/favicons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/backend/img/favicons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/backend/img/favicons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/backend/img/favicons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/backend/img/favicons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/backend/img/favicons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/backend/img/favicons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/backend/img/favicons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/backend/img/favicons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/backend/img/favicons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/backend/img/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/backend/img/favicons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/backend/img/favicons/favicon-16x16.png">
<meta name="msapplication-TileImage" content="/backend/img/favicons/ms-icon-144x144.png">
<link rel="shortcut icon" href="/backend/img/favicons/favicon.png">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
<link rel="stylesheet" id="css-main" href="/backend/css/berlis.css">
<link rel="stylesheet" id="css-main" href="/assets/css/eddy.css">
</head>