<nav id="sidebar">
    <div id="sidebar-scroll">
        <div class="sidebar-content">
            <div class="side-header side-content bg-white-op">
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                
                <a class="h5 text-white" href="/one">
                    <span class="h4 font-w600 sidebar-mini-hide">BERLIS</span>
                </a>
            </div>
            <div class="side-content">
                <ul class="nav-main">
                    
                    <li>
                        <a href="/one/products">
                            <i class="si si-basket-loaded"></i>
                            <span class="sidebar-mini-hide">Products</span>
                        </a>
                    </li>
                    <li>
                        <a href="/one/categories">
                            <i class="si si-grid"></i>
                            <span class="sidebar-mini-hide">Categories</span>
                        </a>
                    </li>
                    <li>
                        <a href="/one/orders">
                            <i class="si si-handbag"></i>
                            <span class="sidebar-mini-hide">Orders</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="javascript:void(0)">
                            <i class="si si-users"></i>
                            <span class="sidebar-mini-hide">Users</span>
                        </a>
                        <ul>
                            <li><a href="">All</a></li>
                            <li><a href="">Active</a></li>
                            <li><a href="">Inactive</a></li>
                            <li><a href="">Blocked</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="javascript:void(0)">
                            <i class="si si-settings"></i>
                            <span class="sidebar-mini-hide">Tools</span>
                        </a>
                        <ul>
                            <li><a href="/one/files">Files</a></li>
                            <li><a href="">Slider</a></li>
                            <li><a href="">Report</a></li>
                            <li><a href="">Reviews</a></li>
                            <li><a href="">Settings</a></li>
                            <li><a href="/one/variations">Variations</a></li>
                        </ul>
                    </li>


                    @if(Auth::check())
                    <li class="nav-main-heading"><span class="sidebar-mini-hide">My Account</span></li>
                    <li>
                        <a href="/one/account">
                            <i class="si si-user"></i>
                            <span class="sidebar-mini-hide">{{ Auth::user()->firstname }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/one/signout">
                            <i class="si si-power"></i>
                            <span class="sidebar-mini-hide">Sign Out</span>
                        </a>
                    </li>
                    @endif

                     <li class="nav-main-heading"><span class="sidebar-mini-hide">More</span></li>
                     <li>
                        <a href="/" target="_blank">
                            <i class="si si-paper-plane"></i>
                            <span class="sidebar-mini-hide">Main Website</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>