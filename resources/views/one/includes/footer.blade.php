
<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-white clearfix">
    <div class="pull-right">
        <a class="font-w600" href="javascript:void(0)" target="_blank">Berlis</a> &copy; {{ date('Y') }}
    </div>
</footer>