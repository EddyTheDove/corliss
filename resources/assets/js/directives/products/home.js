(function() {
    'user strict';

    angular
    .module('app.directives')
    .directive('homeProducts', function() {
        return {
            scope: true,
            restrict: 'E',
            replace: true,
            templateUrl: '/views/products/home.html',
            link: function($scope, element, $attrs, ngModel) {

            }
        }
    })
})();
