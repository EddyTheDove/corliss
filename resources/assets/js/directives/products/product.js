(function() {
    'user strict';

    angular
    .module('app.directives')
    .directive('product', function($timeout, $interval) {
        return {
            scope: true,
            restrict: 'E',
            replace: true,
            templateUrl: '/views/products/product.html',
            link: function($scope, element, $attrs, ngModel) {

            }
        }
    })
})();
