(function() {
    'user strict';

    angular
    .module('app.directives')
    .directive('products', ['$timeout', '$interval', function($timeout, $interval) {
        return {
            scope: true,
            restrict: 'E',
            replace: true,
            templateUrl: '/views/products/products.html',
            link: function($scope, element, $attrs, ngModel) {

            }
        };
    }]);
})();
