(function() {
    'user strict';

    angular
    .module('app.controllers')
    .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'ApiService']
    function HomeController($scope, ApiService)
    {
        var vm = this;
        activate();

        function activate()
        {
            $scope.prime    = _prime;
            $scope.products = _products;


            $scope.like = function(item) {
                item.is_favourite = !item.is_favourite
            }
        }
    }
})();
