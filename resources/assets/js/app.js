(function() {
    'use strict';

    var __env = {};
    if (window) {
        Object.assign(__env, window.__env);
    }

    angular
    .module('berlish', [
        'app.core',
        'app.controllers',
        'app.directives'
    ])
    .constant('__env', __env)
    .config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider
            .startSymbol('{[')
            .endSymbol(']}');
    }])
    .run(['$http', '$rootScope', function($http, $rootScope) {
        var token = 'Bearer ' + __berlish;
        $http.defaults.headers.common.Authorization = token;
    }]);
})();
