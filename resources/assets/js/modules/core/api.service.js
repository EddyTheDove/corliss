(function(){
    'use strict';
    angular
    .module('app.core')
    .service('ApiService', ApiService);

    ApiService.$inject = ['$http', '$rootScope', '__env'];
    function ApiService($http, $rootScope, __env)
    {
        //Gestion des erreurs
        function handleError(error, errorCallback)
        {
            if(errorCallback) {
                $rootScope.loading = false;
                return errorCallback(error);
            }
            console.log(error);
            $rootScope.loading = false;
        }


        //HTTP GET
        this.get = function(url, successCallback, errorCallback)
        {
            $rootScope.loading = true;
            return $http.get(__env.endPoint + url)
            .then(function(response) {
                $rootScope.loading = false;
                return successCallback(response);
            }, function(error) {
                return handleError(error, errorCallback);
            });
        }


        //HTTP POST
        this.post = function(url, data, successCallback, errorCallback)
        {
            $rootScope.loading = true;
            return $http({
                method: 'POST',
                url: __env.endPoint + url,
                data: data
            })
            .then(function(response) {
                $rootScope.loading = false;
                return successCallback(response);
            }, function(error) {
                return handleError(error, errorCallback);
            });
        }



        //HTTP UPDATE
        this.update = function(url, data, successCallback, errorCallback)
        {
            $rootScope.loading = true;
            return $http({
                method: 'PUT',
                url: __env.endPoint + url,
                data: data
            })
            .then(function(response) {
                $rootScope.loading = false;
                return successCallback(response);
            }, function(error) {
                return handleError(error, errorCallback);
            });
        }



        //HTTP DELETE
        this.delete = function(url, successCallback, errorCallback)
        {
            return $http.delete(__env.endPoint + url)
            .then(function(response) {
                $rootScope.loading = false;
                return successCallback(response);
            }, function(error) {
                return handleError(error, errorCallback);
            });
        }
    }
})();
