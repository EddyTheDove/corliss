(function(){
    'use strict';
    angular
        .module('app.core', [
            'ui.bootstrap',
            'ngSanitize',
            'ngAnimate',
            'ngDialog',
            'toaster',
            'angularMoment',
            'ngRoute'
        ])
})();
